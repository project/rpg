<?php

function rpg_drudge_rpg_type_tangible() {
  $type = array();
  $type['#type'] = 'tangible';
  $type['#name'] = t('Tangible object');
  $type['#shortdesc'] = t('Tangible objects have a physical existance, and may be examined, touched, and usually taken.');
  $type['#longdesc'] = $type['#shortdesc'];
  $type['#parents'] = array();
  $type['#attributes'] = array();
  $type['#attributes']['name'] = array(
    '#name' => t('name'),
    '#class' => 'text',
    '#description' => t('This is the human readable name of the object.'),
  );
  $type['#attributes']['plural'] = array(
    '#name' => t('plural'),
    '#class' => 'text',
    '#description' => t('This is the plural name of this object, when there is more than one.'),
    '#get' => t('_rpg_drudge_get_plural'),
  );
  $type['#attributes']['title'] = array(
    '#name' => t('title'),
    '#class' => 'text',
    '#description' => t('This is the title of an object. Generally, this will simply be the name of the object. If specified, then that will be returned instead.'),
    '#get' => t('_rpg_drudge_get_title'),
  );
  $type['#attributes']['description'] = array(
    '#name' => t('description'),
    '#class' => 'text',
    '#description' => t('This is the description of the object.'),
  );
  $type['#attributes']['is_plural'] = array(
    '#name' => t('is plural'),
    '#class' => 'boolean',
    '#default' => FALSE,
    '#description' => t('Set this to TRUE if the object is a collective of other objects.'),
  );
  $type['#attributes']['location'] = array(
    '#name' => t('location'),
    '#class' => 'object', // this determines mainly how the attribute tables are created for objects, but may also affect processing
    '#description' => t('This attribute keeps track of the object containing this one. Every tangible object is generally located in another object. Some may not be, such as top containing rooms -- they are in a sort of undefined limbo.'),
    '#verify' => '_rpg_drudge_location_verify', // this function will be called before setting the attribute, and must return TRUE to continue
    '#set' => '_rpg_drudge_location_set', // this function will be called when setting the attribute on an object
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['contents'] = array(
    '#name' => t('contents'),
    '#class' => 'object_array',
    '#description' => t('This is an array of all the objects contained directly in this object.'),
    '#form_display' => '_rpg_drudge_form_display_FALSE',
  );
  $type['#attributes']['moveable'] = array(
    '#name' => t('moveable'),
    '#class' => 'boolean',
    '#description' => t('If an object is moveable, then it may be taken by an actor. Objects not moveable are generally fixed, too heavy, or part of the scenery.'),
    '#default_value' => TRUE, // objects of this type will begin with this value for the attribute
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['visible'] = array(
    '#name' => t('visible'),
    '#class' => 'boolean',
    '#description' => t('Can this object be seen in normal circumstances?'),
    '#default_value' => TRUE,
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['light'] = array(
    '#name' => t('light'),
    '#class' => 'number',
    '#description' => t('What light does this object cast? 0 is none, 1 is dim, 2 is average, and 3 is bright. 1 creates light enough to see the object but nothing else. 2 creates enough light to see everything in a room. 3 creates enough light to be seen from a distance.'),
    '#default_value' => 0,
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['listable'] = array(
    '#name' => t('listable'),
    '#class' => 'boolean',
    '#description' => t("Will this object be listed in its location's contents?"),
    '#default_value' => TRUE,
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['listable_contents'] = array(
    '#name' => t('listable contents'),
    '#class' => 'figured',
    '#description' => t('This returns a list of contents of this object that may be listed in the contents.'),
    '#get' => '_rpg_drudge_listable_contents',
  );
  $type['#attributes']['nested_contents'] = array(
    '#name' => t('nested contents'),
    '#class' => 'figured',
    '#get' => '_rpg_drudge_nested_contents',
    '#description' => t('Return all contents within the container, and any nested contents of those.'),
  );
  $type['#attributes']['may_see'] = array(
    '#name' => t('may see'),
    '#class' => 'figured',
    '#description' => t("Is the actor able to currently see this object? Based on whether it's visible, in the same room, not in a closed box, etc."),
    '#get' => '_rpg_drudge_may_see',
  );
  $type['#attributes']['may_touch'] = array(
    '#name' => t('may touch'),
    '#class' => 'figured',
    '#description' => t("Is the actor able to currently touch this object? Based on whether it's in reach in the same room, visible, not in a closed box, etc."),
    '#get' => '_rpg_drudge_may_touch',
  );
  $type['#attributes']['may_take'] = array(
    '#name' => t('may take'),
    '#class' => 'figured', // figured class attributes may not be manually set
    '#description' => t('May an object be taken by the actor? This will return TRUE if the object is contained within the same room as the actor, is visible, may be touched, is moveable, and is not possessed by another actor.'),
    '#get' => '_rpg_drudge_may_take', // this function will be called to return the value of an attribute.
  );
  $type['#attributes']['may_attack'] = array(
    '#name' => t('may attack'),
    '#class' => 'figured',
    '#description' => t('Are you able to attack this object?'),
    '#get' => '_rpg_drudge_may_attack',
  );
  $type['#attributes']['is_directly_in'] = array(
    '#name' => t('is directly in'),
    '#class' => 'figured',
    '#description' => t('Is the object directly within the container being tested for?'),
    '#get' => '_rpg_drudge_is_directly_in',
  );
  $type['#attributes']['is_in'] = array(
    '#name' => t('is in'),
    '#class' => 'figured',
    '#description' => t('Is the object contained within the container, or within another object in the container?'),
    '#get' => '_rpg_drudge_is_in',
  );
  $type['#attributes']['is_in_room_with'] = array(
    '#name' => t('is in room with'),
    '#class' => 'figured',
    '#description' => t('Is the object in the same room with the object being tested for?'),
    '#get' => '_rpg_drudge_is_in_room_with',
  );
  $type['#attributes']['top_room'] = array(
    '#name' => t('top room'),
    '#class' => 'figured',
    '#description' => t('What is the top container/room for this object?'),
    '#get' => '_rpg_drudge_top_room',
  );
  $type['#actions'] = array();
  $type['#actions']['examine'] = array(
    '#name' => t('examine'),
    '#php' => '_rpg_drudge_examine', // this is the function called when using this action
    '#description' => t("Generally, when a player is in the same room as an object, it may be examined for more details."),
    '#expose' => '_rpg_drudge_examine_expose',
  );
  $type['#actions']['take'] = array(
    '#name' => t('take'),
    '#php' => '_rpg_drudge_take', // this is the function called when using this action
    '#expose' => '_rpg_drudge_take_expose',
    '#description' => t("This action will take the object and place it in the actor's inventory."),
  );
  $type['#actions']['drop'] = array(
    '#name' => t('drop'),
    '#php' => '_rpg_drudge_drop',
    '#expose' => '_rpg_drudge_drop_expose',
    '#description' => t("This action will drop the object and place it in the actor's location."),
  );
  $type['#events'] = array();
  $type['#events']['on_destroy'] = array(
    '#name' => t('on destroy'),
    '#description' => t("When an object is destroyed, move its contents to its container, and remove it from its location."),
    '#php' => '_rpg_drudge_tangible_on_destroy',
  );
  return $type;
}

function rpg_drudge_rpg_type_unmoveable() {
  $type = array();
  $type['#type'] = 'unmoveable';
  $type['#name'] = t('Unmoveable');
  $type['#shortdesc'] = t('An unmoveable object simply may not be moved.');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('tangible');

  $type['#attributes'] = array();
  $type['#attributes']['moveable'] = array(
    '#name' => t('moveable'),
    '#class' => 'boolean',
    '#description' => t('If an object is moveable, then it may be taken by an actor. Objects not moveable are generally fixed, too heavy, or part of the scenery.'),
    '#default_value' => FALSE,
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  return $type;
}

function rpg_drudge_rpg_type_actor() {
  $type = array();
  $type['#type'] = 'actor';
  $type['#name'] = t('Actor');
  $type['#shortdesc'] = t('The Actor is the base class for mobile creatures, such as humans, goblins, orcs, animals, etc.');
  $type['#longdesc'] = $type['#shortdesc'];


  $type['#parents'] = array('tangible');

  $type['#attributes'] = array();
  $type['#attributes']['pc'] = array(
    '#name' => t('PC'),
    '#class' => 'user',
    '#description' => t('If set to a Drupal User, then this actor may be chosen to play as a Player Character (PC).'),
    '#set' => '_rpg_drudge_set_pc',
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['is_a_pc'] = array(
    '#name' => t('is a PC'),
    '#class' => 'figured',
    '#description' => t('Is this actor a PC character?'),
    '#get' => '_rpg_drudge_is_a_pc',
  );
  $type['#attributes']['is_pc'] = array(
    '#name' => t('is pc'),
    '#class' => 'figured',
    '#description' => t('Is this actor the current PC?'),
    '#get' => '_rpg_drudge_is_pc',
  );
  $type['#attributes']['is_listener'] = array(
    '#name' => t('is listener'),
    '#class' => 'figured',
    '#description' => t('Is this actor able to listen to messages?'),
    '#get' => '_rpg_drudge_is_listener',
  );
  $type['#attributes']['moveable'] = array(
    '#name' => t('moveable'),
    '#class' => 'boolean',
    '#description' => t('Actors are generally unmoveable by other actors, with the exception of small actors such as mice.'),
    '#default_value' => FALSE, // objects of this type will begin with this value for the attribute
    '#form_display' => '_rpg_drudge_form_display_admin',
  );

  $type['#actions'] = array();
  $type['#actions']['inventory'] = array(
    '#name' => t('inventory'),
    '#php' => '_rpg_drudge_display_inventory',
    '#description' => t('This returns a list of the objects held by the actor.'),
    '#expose' => '_rpg_drudge_self_action_expose',
  );
  $type['#actions']['look_around'] = array(
    '#name' => t('look around'),
    '#php' => '_rpg_drudge_look_around',
    '#description' => t('Get a good look around the place.'),
    '#expose' => '_rpg_drudge_self_action_expose',
  );

  $type['#events'] = array();

  return $type;
}


/**
 *  this creates a basic race/class to be used as player characters
 */
function rpg_drudge_rpg_type_pc() {
  $type = array();
  $type['#type'] = 'pc';
  $type['#name'] = t('Player Character');
  $type['#shortdesc'] = t('Types descending from this type will automatically be registered as Player Characters (PCs) when created.');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('actor');

  $type['#attributes'] = array();
  $type['#attributes']['pc'] = array(
    '#name' => t('PC'),
    '#class' => 'user',
    '#description' => t('If set to a Drupal User, then this actor may be chosen to play as a Player Character (PC). This defaults to the creating user.'),
    '#set' => '_rpg_drudge_set_pc_default_user',
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['current_pc_page'] = array(
    '#name' => t('Current PC Page'),
    '#class' => 'text',
    '#description' => t('What page of the character creation process are we in?'),
    '#form_display' => '_rpg_drudge_form_display_admin',
  );

  $type['#actions'] = array();

  return $type;
}


/**
 *  rooms to contain everything
 */
function rpg_drudge_rpg_type_room() {
  $type = array();
  $type['#type'] = 'room';
  $type['#name'] = t('Room');
  $type['#shortdesc'] = t('Rooms contain objects, including characters, and may be viewed from inside.');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('tangible', 'unmoveable');

  $type['#attributes'] = array();
  $type['#attributes']['title'] = array(
    '#name' => 'title',
    '#class' => 'text',
    '#description' => t('The room\'s title. Will be \'A Dark Room\' if there is no light.'),
    '#get' => '_rpg_drudge_get_room_title',
  );
  $type['#attributes']['dark_room_name'] = array(
    '#name' => 'dark room name',
    '#class' => 'text',
    '#description' => t('The name of the room when it is dark.'),
    '#default' => t('A Dark Room'),
  );
  $type['#attributes']['dark_room_desc'] = array(
    '#name' => 'dark room desc',
    '#class' => 'text',
    '#description' => t('The description of the room when it is dark.'),
    '#default' => t('You can see nothing.'),
  );
  $type['#attributes']['description'] = array(
    '#name' => 'description',
    '#class' => 'text',
    '#description' => t('The room\'s description. Will be \'You can see nothing\' if there is no light.'),
    '#get' => '_rpg_drudge_get_room_description',
  );
  $type['#attributes']['exits'] = array(
    '#name' => t('exits'),
    '#class' => 'figured',
    '#description' => t('This is a listing of exits leading from a room.'),
    '#get' => '_rpg_drudge_room_exits',
  );
  $type['#attributes']['dest'] = array(
    '#name' => t('destination'),
    '#class' => 'object',
    '#allowed_values' => array('room'),
    '#get' => t('_rpg_drudge_room_get_dest'),
    '#description' => t('A room will have a raw destination of itself, so that exits may link directly to the room, rather than to a matching exit.'),
  );
  $type['#attributes']['listeners'] = array(
    '#name' => t('listeners'),
    '#class' => 'figured',
    '#description' => t('A list of contents of the room able to receive observer messages within the room.'),
    '#get' => '_rpg_drudge_room_listeners',
  );
  $type['#attributes']['light'] = array(
    '#name' => t('light'),
    '#class' => 'number',
    '#description' => t('What light does this object cast? 0 is none, 1 is dim, 2 is average, and 3 is bright. 1 creates light enough to see the object but nothing else. 2 creates enough light to see everything in a room. 3 creates enough light to be seen from a distance.'),
    '#default_value' => 2,
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#attributes']['brightness'] = array(
    '#name' => t('brightness'),
    '#class' => 'figured',
    '#description' => t('How much ambient light is in a room? This will be either the light level of the room, or the highest of the room\'s light level and the light level of any contained object with a brightness of at least 2.'),
    '#form_display' => '_rpg_drudge_form_display_admin',
    '#get' => '_rpg_druge_room_brightness',
  );

  $type['#actions'] = array();
  $type['#actions']['dig'] = array(
    '#name' => t('dig'),
    '#php' => '_rpg_drudge_room_dig',
    '#expose' => '_rpg_drudge_room_dig_expose',
    '#description' => t('Admins will be able to dig new exits into other rooms.'),
  );

  $type['#events'] = array();
  $type['#events']['on_create'] = array(
    '#name' => t('on create'),
    '#php' => '_rpg_drudge_room_oncreate',
    '#description' => t('When rooms are created, their destination is set to themselves, so that exits may link directly to the room.'),
  );

  return $type;
}


/**
 *  exits to enter rooms from
 */
function rpg_drudge_rpg_type_exit() {
  $type = array();
  $type['#type'] = 'exit';
  $type['#name'] = t('Exit');
  $type['#shortdesc'] = t('Exits connect rooms together, and may be traversed.');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('tangible', 'unmoveable');

  $type['#attributes'] = array();
  $type['#attributes']['dest'] = array(
    '#name' => t('dest'),
    '#class' => 'object',
    '#allowed_values' => array('room'),
    '#description' => t('The raw destination of an exit is the room that will be entered after transiting it, or the matching exit.'),
  );
  $type['#attributes']['listable'] = array(
    '#name' => t('listable'),
    '#class' => 'boolean',
    '#description' => t("Exits won't be listed in its room's contents."),
    '#default_value' => FALSE,
  );
  $type['#attributes']['may_traverse'] = array(
    '#name' => t('may traverse'),
    '#class' => 'figured',
    '#description' => t('Is the actor able to traverse this exit?'),
    '#get' => '_rpg_drudge_may_traverse'
  );
  $type['#attributes']['destination'] = array(
    '#name' => t('destination'),
    '#class' => 'figured',
    '#description' => t('The figured destination is where an object will end up after following through this destination. Usually equal to the room of dest, or if it\'s an exit, then that exit\'s location.'),
    '#get' => '_rpg_drudge_destination',
  );

  $type['#actions'] = array();
  $type['#actions']['go'] = array(
    '#name' => t('go'),
    '#php' => '_rpg_drudge_go',
    '#expose' => '_rpg_drudge_go_expose',
    '#description' => t('Going through an exit will take you to the connecting room'),
  );

  $type['#events'] = array();

  return $type;
}


/**
 *  Compass rose exits
 */
function rpg_drudge_rpg_type_compass_exit() {
  $type = array();
  $type['#type'] = 'compass_exit';
  $type['#name'] = t('Compass exit');
  $type['#shortdesc'] = t('Compass exits follow a traditional or nautical compass rose, such as north, east, southwest, port, or aft.');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('exit');

  $type['#attributes'] = array();
  $type['#attributes']['direction'] = array(
    '#name' => t('direction'),
    '#class' => 'text',
    '#allowed_values' => array(t('north'), t('east'), t('west'), t('south'), t('northeast'), t('northwest'), t('southeast'), t('southwest'), t('in'), t('out'), t('up'), t('down'), t('fore'), t('aft'), t('port'), t('starboard')),
    '#description' => t('This is the direction of the compass rose this exit points, such as north, east, southwest, etc. Allowed values are north, east, west, south, northeast, northwest, southeast, southwest, in, out, up, down, fore, aft, port, and starboard (for ships).'),
  );

  $type['#actions'] = array();

  $type['#events'] = array();

  return $type;
}

/**
 *  countable objects have a count, such as gold pieces, arrows, or gumballs. an object of this count will clone itself
 *  when divided, and destroy any others in the same container when adding, adding or subtrcting from the count as necessary.
 *  it will destroy itself it is count ever becomes zero.
 */
function rpg_drudge_rpg_type_countable() {
  $type = array();
  $type['#type'] = 'countable';
  $type['#name'] = t('Countable');
  $type['#shortdesc'] = t('Countable objects have a count, such as gold pieces, arrows, or gumballs');
  $type['#longdesc'] = $type['#shortdesc'];

  $type['#parents'] = array('tangible');

  $type['#attributes'] = array();
  $type['#attributes']['count'] = array(
    '#name' => t('count'),
    '#class' => 'number',
    '#description' => t('How many are there?'),
    '#set' => '_rpg_drudge_set_count',
  );

  $type['#actions'] = array();
  $type['#attributes']['title'] = array(
    '#name' => t('title'),
    '#class' => 'text',
    '#description' => t('The title of a countable object will include the count of that object.'),
    '#get' => t('_rpg_drudge_get_countable_title'),
  );
  $type['#attributes']['location'] = array(
    '#name' => t('location'),
    '#class' => 'object', // this determines mainly how the attribute tables are created for objects, but may also affect processing
    '#description' => t('When more than one countable objects are in the same location, combine them.'),
    '#verify' => '_rpg_drudge_location_verify', // this function will be called before setting the attribute, and must return TRUE to continue
    '#set' => '_rpg_drudge_countable_location_set', // this function will be called when setting the attribute on an object
    '#form_display' => '_rpg_drudge_form_display_admin',
  );
  $type['#actions']['combine'] = array(
    '#name' => t('combine'),
    '#php' => '_rpg_drudge_combine',
    '#expose' => '_rpg_drudge_combine_expose',
    '#description' => t('This will combine two identical countable objects.'),
  );
  $type['#actions']['divide'] = array(
    '#name' => t('divide'),
    '#php' => '_rpg_drudge_divide',
    '#expose' => '_rpg_drudge_divide_expose',
    '#description' => t('This will divide a countable object into two identical countable objects.'),
  );

  $type['#events'] = array();

  return $type;
}

