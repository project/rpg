<?php

/**
 *  rpg.t.inc
 *  this file defines the rpg_t function, which provides for substitution strings for messages displayed to players.
 */

function rpg_t($message, $objects = array(), $translations = array()) {
  global $rpg;

  // default $pc is $rpg['pc']
  if (!isset($objects['pc'])) {
    $objects['pc'] = rpg_object($rpg['pc']);
  }
  else {
    $objects['pc'] = rpg_object($objects['pc']);
  }

  // default $room is pc's room
  if (!isset($objects['room']) && is_object($objects['pc'])) {
    $objects['room'] = rpg_object($objects['pc']->location);
  }
  else if (isset($objects['room'])) {
    $objects['room'] = rpg_object($objects['room']);
  }

  // default $observer is the pc
  // this affects things like 'you' and verb tense
  // this will ultimately be called for all potentially human observers of an event
  if (!isset($objects['observer']) && is_object($objects['pc'])) {
    $objects['observer'] = $objects['pc'];
  }
  else if (isset($objects['observer'])) {
    $objects['observer'] = rpg_object($objects['observer']);
  }

  if (is_object($objects['pc'])) {
    $pc = $objects['pc'];
    if ($pc == $objects['observer']) {
      $translations = array_merge($translations, array(
        '!you' => l(t('you'), 'rpg/view/' . $pc->rid),
        '!You' => l(t('You'), 'rpg/view/' . $pc->rid),
        '!your' => l(t('your'), 'rpg/view/' . $pc->rid),
        '!Your' => l(t('Your'), 'rpg/view/' . $pc->rid),
        '!yours' => l(t('yours'), 'rpg/view/' . $pc->rid),
        '!Yours' => l(t('Yours'), 'rpg/view/' . $pc->rid),
        '!is' => t('are'),
        '!are' => t('are'),
        '!Is' => t('Are'),
        '!Are' => t('Are'),
        '!s' => '',
      ));
    } else {
      $translations = array_merge($translations, array(
        '!you' => l($pc->name, 'rpg/view/' . $pc->rid),
        '!You' => l($pc->name, 'rpg/view/' . $pc->rid),
        '!your' => l($pc->name . '\'s', 'rpg/view/' . $pc->rid),
        '!Your' => l($pc->name . '\'s', 'rpg/view/' . $pc->rid),
        '!yours' => l($pc->name . '\'s', 'rpg/view/' . $pc->rid),
        '!Yours' => l($pc->name . '\'s', 'rpg/view/' . $pc->rid),
        '!is' => t('is'),
        '!are' => t('is'),
        '!Is' => t('Is'),
        '!Are' => t('Is'),
        '!s' => 's',
      ));
    }
    // pronouns -- TODO: assign to genders & link
    $translations = array_merge($translations, array(
      '!he' => t('it'), // he/she/it plays
      '!she' => t('it'), // he/she/it plays
      '!him' => t('it'), // give the ball to him/her/it
      '!her' => t('it'), // give the ball to him/her/it
      '!his' => t('its'), // that is his/her/its ball
      '!hers' => t('its'), // that ball is his/hers/its
      '!himself' => t('itself'), // he looks at himself/herself/itself
      '!herself' => t('itself'), // he looks at himself/herself/itself
      '!He' => t('It'),
      '!She' => t('It'),
      '!Her' => t('It'),
      '!His' => t('Its'),
      '!Hers' => t('Its'),
      '!Himself' => t('Itself'),
      '!Herself' => t('Itself'),
    ));
  }
  if (is_object($objects['obj'])) {
    $obj = $objects['obj'];
    $translations = array_merge($translations, array(
      '!obj' => l($obj->name, 'rpg/view/' . $obj->rid),
      '!Obj' => l(ucfirst($obj->name), 'rpg/view/' . $obj->rid),
      '!theobj' => rpg_t_thename($obj),
      '!Theobj' => ucfirst(rpg_t_thename($obj)),
    ));
  }
  if (is_object($objects['dobj'])) {
    $dobj = $objects['dobj'];
    $translations = array_merge($translations, array(
      '!dobj' => l($dobj->name, 'rpg/view/' . $dobj->rid),
      '!Dobj' => l(ucfirst($dobj->name), 'rpg/view/' . $dobj->rid),
      '!thedobj' => rpg_t_thename($dobj),
      '!Thedobj' => ucfirst(rpg_t_thename($dobj)),
    ));
  }
  if (is_object($objects['room'])) {
    $room = $objects['room'];
    $translations = array_merge($translations, array(
      '!theroom' => rpg_t_thename($room),
      '!Theroom' => ucfirst(rpg_t_thename($room)),
    ));
  }
  return t($message, $translations);
}

function rpg_t_thename($object) {
  $object = rpg_object($object);
  if (rpg_get('proper', $object)) {
    return l(rpg_get('name', $object), 'rpg/view/' . $object->rid);
  }
  return t('the !object', array('!object' => l(rpg_get('name', $object), 'rpg/view/' . $object->rid)));
}

