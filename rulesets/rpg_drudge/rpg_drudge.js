
/**
 *  This will cause the clicked section to slide open, and close the previous section.
 *
 *  _rpg_drudge_block_actions_slider_current is set in the document, and begins as either 0
 *  or the current section, equal to arg(2), when it's rpg/[arg]/[rid].
 *
 *  @param _slider
 *  This is the #id of the section. We don't slide if we click the current slider.
 *  The clicked section becomes the current slider.
 */
function rpg_drudge_block_actions_slider_toggle(_slider) {
  if (_slider != _rpg_drudge_block_actions_slider_current) {
    $(_rpg_drudge_block_actions_slider_current).slideUp('fast');
    $(_slider).slideDown('fast');
  }
  _rpg_drudge_block_actions_slider_current = _slider;
}

/**
 *  This will iterate through each action block and attach a toggle slider event on each title.
 *  Also will hide all those except the active title (_rpg_drudge_block_actions_slider_current)
 *
 *  _rpg_drudge_block_actions_slider_current is set in the document, and begins as either 0
 *  or the current section, equal to arg(2), when it's rpg/[arg]/[rid].
 *
 *  _rpg_drudge_block_actions_slider_size is the count of sections in the block.
 */
$(document).ready(function() {
  for (i = 0; i < _rpg_drudge_block_actions_slider_size; i++) {
    _slider_div = '#rpg-drudge-block-actions-slider-content-' + i
    if (_slider_div != _rpg_drudge_block_actions_slider_current) {
      $(_slider_div).hide();
    }
    $('#rpg-drudge-block-actions-slider-title-' + i).attr('rpg-drudge-block-actions-slider', _slider_div);
    $('#rpg-drudge-block-actions-slider-title-' + i).click(function () {
      rpg_drudge_block_actions_slider_toggle($(this).attr('rpg-drudge-block-actions-slider'));
    });
  }
});

