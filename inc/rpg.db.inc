<?php

/**
 *  @file
 *  these are functions interfacing with the database for storing rpg objects
 */

/**
 *  TODO: update this function for the new api
 *  this loads an rpg object into the global $rpg array as $rpg[$rid]
 *  this should rarely be called directly, as it returns nothing; instead, call rpg_object
 *    @param $rid
 *      the integer object id of the object to load
 *    @param $force
 *      optional -- if true, force the object to be reloaded. otherwise, stored in a static array.
 */
function _rpg_load_rpg($rid, $force = FALSE) {
  global $rpg;

  if ($rid && is_numeric($rid) && (!isset($rpg[$rid]) || $force)) {
    $sql = "SELECT * FROM {rpg} WHERE rid=%d";
    $results = db_query($sql, $rid);
    $object = db_fetch_object($results);
    if ($object->rid) {
      $object->types = array();
      $sql = "SELECT type from {rpg_object_types} WHERE rid=%d";
      $results = db_query($sql, $object->rid);
      while ($type = db_fetch_array($results)) {
        $object->types[] = $type['type'];
      }
      // TODO: change this to drupal_alter for d6

      // does this actually need to happen?
//      rpg_invoke_object($object, 'load');
    }
    $rpg[$rid] = $object;
    rpg_drupal_alter('rpg_object', $object, 'load');
  }
  else if ($rid === 0) {
    $rpg[$rid] = rpg_limbo();
  }
}

/**
 *  TODO: update this function for the new api.
 *  TODO: it's not actually called anywhere any more.
 */
function rpg_limbo() {
  static $limbo;
  if (!isset($limbo)) {
    $limbo = array(
      'types' => array(),
      'uid' => 1,
      'rid' => 0,
      'data' => array(),
//       'location' => 0,
//       'contents' => array(),
//       'name' => t('limbo'),
//       'data' => array('proper' => true,),
    );
    $limbo = (object) $limbo;
  }
  return $limbo;
}

/**
 *  TODO: update this function for the new api.
 *  this creates a new blank rpg object of $type, created by $account->uid. called by rpg_create_object_page, and
 *  may be called programatically
 *  @param $type
 *    the type array returned by rpg_types
 *  @param $owner
 *    the account of the owning user
 *  @param $auto_save
 *    optional; if true, then we'll automatically save w/ new rid
 */
function rpg_new_rpg_object($type, $owner, $auto_save = false) {
  global $rpg;

  $object = array(
    'types' => array($type['type']),
    'uid' => $owner->uid,
    'is_new' => true,
    'rid' => 'temp:' . rand(),
    'data' => array(),
  );

  // set up initial attribute data. note that this allows for objects to inherit from multiple types
  // also, since default values of inherited types are set first, they will automatically be overridden
  // and the same when inheriting from multiple types. although we should TODO do this from the flattened
  // type array ultimately... see that function in rpg.invoke.inc...
  foreach ($object['types'] as $type_str) {
    $type = rpg_types($type_str);
    foreach ($type['attributes']['inherited'] as $attr => $attribute) {
      foreach ($attribute['types'] as $parent) {
        $object['data'][$attr] = $parent['default_value'];
      }
    }
    foreach ($type['attributes']['defined'] as $attr => $attribute) {
      $object['data'][$attr] = $attribute['default_value'];
    }
  }

  // turn our object into an object
  $object = (object) $object;

  // if we call this function with autosave, then automatically put it into the db.
  // this may be used to quickly add one programatically. not called when creating one with a form.
  if ($auto_save) {
    $object = _rpg_insert_new_rpg($object);
  }

  $rpg[$object->rid] = $object;

  // return the object object to be put into the global $rpg array
  return $object;
}


/**
 *  this inserts a newly created rpg object into the database, assigning a new rid,
 *  adding it to the global $rpg array, and returning the revised object (with the new rid).
 *  this is called by rpg_new_rpg_object when autosave is set. also called from rpg_form_object_submit
 *
 *  @param $object
 *    an object with the basic fields filled in (from a previously called rpg_invoke('create', $object))
 *  @return
 *    the object with the new ->rid
 */
function _rpg_insert_new_rpg($object) {
  global $rpg;
  $object->rid = db_next_id('{rpg}_rid');
  $rpg[$object->rid] = $object;
//  rpg_invoke_object($object, 'insert');
  unset($rpg[$object->rid]->is_new);
  $object = $rpg[$object->rid];
  db_query("INSERT INTO {rpg} (rid, uid, created, changed) VALUES (%d, %d, %d, %d)", $object->rid, $object->uid, $object->created, $object->changed);
  foreach($object->types as $type) {
    db_query("INSERT INTO {rpg_object_types} (rid, type) VALUES (%d, '%s')", $object->rid, $type);
  }

  // make sure each attribute will be initially saved, preserving defaults
  foreach (rpg_object_attributes($object) as $type) {
    if (!$object->data[$type['attribute']] && $type['default_value']) {
      $object->data[$type['attribute']] = $type['default_value'];
    }
  }
  foreach ($object->data as $attr => $value) {
    rpg_set($attr, $object->rid, $value);
  }

//   rpg_trigger('on_create', $object->rid);

  return $object;
}

/**
 *  TODO: update this function for the new api.
 *  this updates the object, saving its new values into the database.
 *  it does NOT save attributes. that happens in _rpg_save_all_marked_objects.
 *  It is called from rpg_form_object_submit
 *  @param
 *    the object or rid to be saved
 *  @return
 *    NULL
 */
function rpg_save_rpg($object) {//print_r($object);
  global $rpg;
  rpg_invoke_object($object, 'update');//print_r($object);
  $rpg[$object->rid] = $object;
  db_query("UPDATE {rpg} SET uid=%d, created=%d, changed=%d WHERE rid=%d", $object->uid, $object->created, $object->changed, $object->rid);
  db_query("DELETE FROM {rpg_object_types} WHERE rid=%d", $object->rid);
  foreach($object->types as $type) {
    db_query("INSERT INTO {rpg_object_types} (rid, type) VALUES (%d, '%s')", $object->rid, $type);
  }
  // reset attributes according to new types
  rpg_object_attributes($object, NULL, FALSE);
}

/**
 *  this will save all objects marked with ->save, unsetting that field.
 *
 *  generally, this ensures we save only objects whose values have changed. this function will unset all the save arrays,
 *  so they won't get saved again if called again. this function should only be called once per page, at the end of
 *  page load, or if the global $rpg array is to be reset.
 */
function _rpg_save_all_marked_objects() {
  global $rpg;//print_r($rpg);
  if (is_array($rpg['save'])) {
    foreach ($rpg['save'] as $rid => $attributes) {
      if (is_array($attributes)) {
        // this will give us the object & attributes we need to save...
        foreach ($attributes as $property => $value) {
          _rpg_set_object_value_to_db($rpg[$rid], rpg_attributes($property));
        }
        unset($rpg[$rid]->save);
      }
    }
  }
  unset($rpg['save']);
}

/**
 *  This will set the value of the object's attribute in the database.
 *
 *  The class has a chance to modify the value first. This function is to be ONLY called by rpg_save_all_marked_objects,
 *  which should only be called once per page load (or possibly when resetting the global rpg, if that ever happens).
 *
 *  @param $object
 *  This is the object prepopulated with the correct values. However, the value will be pulled from the global $rpg
 *  before sending to the class for possible modification.
 *  @param $attribute
 *  This is the proper attribute array.
 *  @param $insert
 *  If true, then the value will be inserted into the database. Used for new objects. Otherwise, it's updated instead.
 *  TODO: $insert is NOT CURRENTLY USED -- NEED TO FIGURE BEST WAY TO CALL THAT...
 */
function _rpg_set_object_value_to_db($object, $attribute, $insert = false) {
  global $rpg;
  if ($object->rid && is_numeric($object->rid) && isset($rpg[$object->rid])) {
    $class = rpg_attribute_classes($attribute['class']);
    $value = $rpg[$object->rid]->data[$attribute['attribute']];
    $test_value = module_invoke($class['module'], 'rpg_attribute_class_settings', 'db_set', $class['class'], $value);
    if (isset($test_value)) {
      $value = $test_value;
    }
    foreach (module_invoke($class['module'], 'rpg_attribute_class_settings', 'data', $class['class']) as $data_type => $data) {
      $table = '{rpg_attribute_rpg_' . $attribute['attribute'] . '}';
      if ($data['type'] == 'int') {
        $query_type = '%d';
      }
      else {
        $query_type = '"%s"';
      }
      db_query("DELETE FROM $table WHERE rid=%d", $object->rid);
      db_query("INSERT INTO $table (rid, $data_type) VALUES (%d, $query_type)", $object->rid, $value);
//      if ($insert) {
//        db_queryd("INSERT INTO $table (rid, $data_type) VALUES (%d, $query_type)", $object->rid, $value);
//      }
//      else {
//        db_queryd("UPDATE $table SET $data_type=$query_type WHERE rid=%d", $value, $object->rid);
//      }
    }
  }
}

