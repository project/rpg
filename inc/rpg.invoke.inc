<?php

/**
 * Return an array of installed .inc files and/or loads them upon request.
 * This routine is modeled after drupal_system_listing() (and also depends on it).
 * It's major difference, however, is that it loads .inc files by default.
 *
 * @param $type
 *   Optional; name of the passed object type to find (e.g. "room", "human", etc.).
 * @param $load
 *   Defaults to true; whether to include matching files into memory.
 * @return
 *   An array of file objects optionally matching $type.
 */
function rpg_system_list($type = NULL, $load = true) {
  $files = drupal_system_listing("$type\.inc", drupal_get_path('module', 'rpg')."/types", 'name', 0);

  ksort($files);

  if ($load) {
    foreach ($files as $file) {
      rpg_include_list($file);
    }
  }

  return $files;
}

/**
 * Maintains a list of all loaded include files.
 *
 * @param $file
 *   Optional; a file object (from rpg_system_list()) to be included.
 * @return
 *   An array of all loaded includes (without the .inc extension).
 */
function rpg_include_list($file = NULL) {
  static $list = array();

//  if (!$list) { $list = array(); }

  if ($file && !isset($list[$file->name])) {
    include_once('./'.$file->filename);
    $list[$file->name] = $file->name;
  }

  return $list;
}

/**
 * Determine whether an include implements a hook, cf. module_hook.
 *
 * @param $type
 *   The name of the object type file (without the .inc extension), such as 'tangible' or 'room'.
 * @param $hook
 *   The name of the hook (e.g. "settings", "info", etc.).
 * @return
 *   TRUE if the provider is loaded and the hook is implemented.
 */
function rpg_include_hook($type, $hook) {
  return function_exists('rpg_' . $type .'_'. $hook);
}

/**
 *  Invoke a hook for an object. This will call the hook for all the types & parent types of the object.
 *  Returns the result. Object may be modified in the process.
 *  Note that this should rarely (if ever) called, except during object creation, when we don't have an rid yet
 *  and the object is not yet in the global $rpg system
 *  @param &$object
 *    The object to invoke the hook
 *  @param $hook
 *    The hook to call
 *  @param $args
 *    Any optional or required args to pass to the hook
 *  @return
 *    The return value from the hook, which may be modified at various places (as $pass)
 */
function rpg_invoke_object(&$object) {
  $args = func_get_args();
  // remove $object, since it's passed as a reference
  array_shift($args);
  // $hook is used explicitly in our call later, but we don't care about the rest of the args
  $hook = array_shift($args);
  $pass = NULL;
  foreach (_rpg_invoke_object_types_order($object) as $type) {
    $function = 'rpg_' . $type . '_' . $hook;
    rpg_system_list($type);
    $pass = rpg_include_hook($type, $hook) ? call_user_func_array($function, array(&$object, $pass, $args)) : $pass;
  }
  return $pass;
}

/**
 *  helper function for rpg_invoke:
 *  this returns the order that types for an object should be invoked, which will be in ascending order of depth
 *  for instance, if an object is a door, with a hypothetical type structure of door (exit -- tangible,
 *  lockable -- openable -- tangible)
 *  its order of invocation would be tangible, exit, openable, lockable, door
 *  @param $object
 *    the rpg object being tested
 *  @param $cached
 *    optional; if set to the default of true, results will be fetched from the cache and stored in a static array
 *    we would need to call it with false if we changed the types of an object. in that rare case, you'll need to
 *    call this function manually before rpg_invoke
 *  @return
 *    an array of strings, being a flattened array of types to invoke in order of invocation
 */
function _rpg_invoke_object_types_order($object, $cached = true) {
  static $types = array();
  if (!isset($object->rid) || !isset($types[$object->rid]) || !$cached) {
    if ($object->rid && $cached && $cache = cache_get('_rpg_invoke_object_types_order:' . $object->rid, 'cache_rpg')) {
      $types[$object->rid] = unserialize($cache->data);
    }
    else {
      $depth = 0;
      $object_types = array();
      foreach ((array)$object->types as $type) {
        $object_types[$depth][] = $type;
        _rpg_invoke_type_populate_ancestors(rpg_types($type), $object_types, $depth);
      }
      krsort($object_types);
      $object_types = array_unique(_rpg_invoke_array_flatten($object_types));
      if ($object->rid) {
        $types[$object->rid] = $object_types;
        cache_set('_rpg_invoke_object_types_order:' . $object->rid, 'cache_rpg', serialize($types[$object->rid]));
      } else {
        return $object_types;
      }
    }
  }
  return $types[$object->rid];
}

function _rpg_invoke_type_types_order($type, $reset = false, $provided_types = NULL) {
  static $types = array();
  if (!isset($types[$type['type']]) || $reset) {
    if ($type['type'] && !$reset && $cache = cache_get('_rpg_invoke_type_types_order:' . $type['type'], 'cache_rpg')) {
      $types[$type['type']] = unserialize($cache->data);
    }
    else {
      $depth = 0;
      $type_types = array();
      if (is_array($type['parents'])) {
        foreach ($type['parents'] as $ancestor) {
          $type_types[$depth][] = $ancestor;
          if (!$reset || is_null($provided_types)) {
            $ancestor_type = rpg_types($ancestor);
          }
          else {
            $ancestor_type = is_array($provided_types[$ancestor]) ? $provided_types[$ancestor] : array();
          }
          _rpg_invoke_type_populate_ancestors($ancestor_type, $type_types, $depth, $provided_types);
        }
      }
      krsort($type_types);
      $type_types = array_unique(_rpg_invoke_array_flatten($type_types));
      $types[$type['type']] = $type_types;
      cache_set('_rpg_invoke_object_types_order:' . $type['type'], 'cache_rpg', serialize($types[$type['type']]));
    }
  }
  return $types[$type['type']];
}

/**
 *  this is a helper function for rpg_invoke. it recursively creates a type ancestor tree for a specific object, modifying
 *  the &$types array appropriately
 *  @param $type
 *    the type to return an ancestor tree for
 *  @param &$types
 *    the tree array to recursively modify
 *  @param $depth
 *    the depth of the tree array to fill in
 */
function _rpg_invoke_type_populate_ancestors($type, &$types, $depth = -1, $provided_types = NULL) {
  $depth++;
  if (is_array($type['parents'])) {
    foreach ($type['parents'] as $parent) {
      $types[$depth][$parent] = $parent;
      if (is_null($provided_types)) {
        $ancestor_type = rpg_types($parent);
      }
      else {
        $ancestor_type = is_array($provided_types[$parent]) ? $provided_types[$parent] : array();
      }
      _rpg_invoke_type_populate_ancestors($ancestor_type, $types, $depth, $provided_types);
    }
  }
}

/**
 *  a helper function for rpg_invoke. this returns a flattened multidimensional array
 */
function _rpg_invoke_array_flatten($array) {
    $tmp = array();
    while (($v = array_shift($array)) !== null) {
        if (is_array($v)) {
            $array = array_merge($v, $array);
        } else {
            $tmp[] = $v;
        }
    }
    return $tmp;
}

/**
 *  TODO: is this depcrated?
 *  return an array of all ancestor types for an object, ordered by inheritance
 *  for instance, if an object has types=array('human', 'wizard'), the types returned might be
 *  array('human', 'race', 'actor', 'tangible', 'wizard', 'classed')
 *  while just 'wizard' might return array('wizard', 'classed', 'actor', 'tangible')
 *  @param $object
 *    The object to return all types for
 *  @param $force
 *    Optional. If true, then force a refresh of the otherwise static result
 *  @return
 *    An array of keyed types, ordered by precendence
 */
function rpg_object_all_types($object, $force = false) {
  static $types;
  if (!isset($types)) {
    $types = array();
  }
  if (!isset($types[$object->rid]) || $force) {
    foreach ($object->types as $type) {
      $types[$object->rid][$type] = $type;
      foreach (rpg_type_ancestors($type) as $parent) {
        $types[$object->rid][$parent] = $parent;
      }
    }
  }
  return $types[$object->rid];
}

/**
 *  @file
 *  these are helper functions that control invoking hooks in rpg object types
 */

/**
 * Invoke hook in a particular include.
 *
 * @param $type
 *   The name of the provider (without the .inc extension).
 * @param $hook
 *   The name of the hook (e.g. "info", "extends", etc.).
 * @param ...
 *   Arguments to pass to the hook implementation.
 * @return
 *   The return value of the hook implementation.
 */
function rpg_include_invoke() {
  $args     = func_get_args();
  $type  = array_shift($args);
  $hook     = array_shift($args);
  $function = 'rpg_' . $type . '_' . $hook;
  rpg_system_list($type);
  return rpg_include_hook($type, $hook) ? call_user_func_array($function, $args) : NULL;
}


