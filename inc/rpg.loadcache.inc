<?php

/**
 *  @file
 *  these internal calls control loading types, attributes, actions, and other arrays that rarely need to be built,
 *  instead being cached. Before calling any of these functions, you must call rpg_include_loadcache();
 */

function _rpg_load_resource_types() {
  $resource_types = array();
  foreach (module_implements('load_resource_types') as $module) {
    $types = (array)module_invoke($module, 'load_resource_types');
    foreach ($types as $key => $type) {
      $types[$key]['module'] = $module;
    }
    $resource_types = array_merge($resource_types, $types);
  }
  return $resource_types;
}

/**
 *  implement hook_rpg_load_resource_types
 */
function rpg_rpg_load_resource_types() {
  $resource_types = array();
  $resource_types['node'] = array(
    'type' => 'node',
    'name' => t('Node'),
  );
  return $resource_types;
}

/**
 *  This will load types from the database, and populate them with attributes & actions
 */
function _rpg_type_load_types() {
  $types = array();

  // load _rpg_invoke_type_types_order
  require_once(drupal_get_path('module', 'rpg') . '/inc/rpg.invoke.inc');

  // load all attributes. force them to be reloaded
  $all_attributes = rpg_attributes(NULL, false);

  // load all actions. force them to be reloaded
  $all_actions = rpg_actions(NULL, false);
  // load all events. force them to be reloaded
  $all_events = rpg_events(NULL, false);

  $results = db_query("SELECT * FROM {rpg_types}");
  while ($type = db_fetch_array($results)) {
    // unserialize our arrays
    $type['parents'] = unserialize($type['parents']);

    // add the type to our array
    $types[$type['type']] = $type;

    // load any defined attributes
    $types[$type['type']]['attributes']['defined'] = _rpg_type_load_defined_attributes($type, $all_attributes);

    // load any defined actions
    $types[$type['type']]['actions']['defined'] = _rpg_type_load_defined_actions($type, $all_actions);

    // load any defined events
    $types[$type['type']]['events']['defined'] = _rpg_type_load_defined_events($type, $all_events);
  }

  foreach ($types as $type) {
    $types[$type['type']]['order'] = _rpg_invoke_type_types_order($type, true, $types);
  }

  foreach ($types as $type) {
    $types[$type['type']]['attributes']['inherited'] = _rpg_type_load_inherited_attributes($type, $all_attributes, $types);
    $types[$type['type']]['actions']['inherited'] = _rpg_type_load_inherited_actions($type, $all_actions, $types);
    $types[$type['type']]['events']['inherited'] = _rpg_type_load_inherited_events($type, $all_events, $types);
  }

  return $types;
}

function _rpg_type_load_defined_attributes($type, $all_attributes) {
  $attributes = array();
  foreach ($all_attributes as $attribute) {
    if (isset($attribute['types'][$type['type']])) {
      $attributes[$attribute['attribute']] = $attribute;
      $types_table = 'rpg_attribute_types_' . $attribute['attribute'];
      $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
      $extras['set'] = $extras['_set'];
      unset($extras['_set']);

      $class = rpg_attribute_classes($attribute['class']);
      foreach ((array)module_invoke($class['module'], 'rpg_attribute_class_settings', 'save serialize', $class['class']) as $key => $column) {
        $extras[$column] = unserialize($extras[$column]);
      }
      $attributes[$attribute['attribute']] = array_merge($attributes[$attribute['attribute']], $extras);
    }
  }
  return $attributes;
}

function _rpg_type_load_inherited_attributes($type, $all_attributes, $types) {
  $attributes = array();
  foreach ($type['order'] as $ancestor) {
    $ancestor = $types[$ancestor];
    foreach ($all_attributes as $attribute) {
      if (isset($attribute['types'][$ancestor['type']])) { // was also  && !isset($attributes[$attribute['attribute']])
        $attribute['inherited'] = $ancestor['type'];
        $attributes[$attribute['attribute']] = $attribute;
        $types_table = 'rpg_attribute_types_' . $attribute['attribute'];
        $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
        $extras['set'] = $extras['_set'];
        unset($extras['_set']);

        $class = rpg_attribute_classes($attribute['class']);
        foreach ((array)module_invoke($class['module'], 'rpg_attribute_class_settings', 'save serialize', $class['class']) as $key => $column) {
          $extras[$column] = unserialize($extras[$column]);
        }
        $attributes[$attribute['attribute']] = array_merge($attributes[$attribute['attribute']], $extras);
      }
    }
  }
  return $attributes;
}

function _rpg_type_load_defined_actions($type, $all_actions) {
  $actions = array();
  foreach ($all_actions as $action) {
    if (isset($action['types'][$type['type']])) {
      $actions[$action['action']] = $action;
      $types_table = 'rpg_action_types_' . $action['action'];
      $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
      $actions[$action['action']] = array_merge($actions[$action['action']], $extras);
    }
  }
  return $actions;
}

function _rpg_type_load_defined_events($type, $all_events) {
  $events = array();
  foreach ($all_events as $event) {
    if (isset($event['types'][$type['type']])) {
      $events[$event['event']] = $event;
      $types_table = 'rpg_event_types_' . $event['event'];
      $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
      $events[$event['event']] = array_merge($events[$event['event']], $extras);
    }
  }
  return $events;
}

function _rpg_type_load_inherited_actions($type, $all_actions, $types) {
  $actions = array();
  foreach ($type['order'] as $ancestor) {
    $ancestor = $types[$ancestor];
    foreach ($all_actions as $action) {
      if (isset($action['types'][$ancestor['type']])) { // was also  && !isset($attributes[$attribute['attribute']])
        $action['inherited'] = $ancestor['type'];
        $actions[$action['action']] = $action;
        $types_table = 'rpg_action_types_' . $action['action'];
        $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
        $extras = is_array($extras) ? $extras : array();
        $actions[$action['action']] = array_merge($actions[$action['action']], $extras);
      }
    }
  }
  return $actions;
}

function _rpg_type_load_inherited_events($type, $all_events, $types) {
  $events = array();
  foreach ($type['order'] as $ancestor) {
    $ancestor = $types[$ancestor];
    foreach ($all_events as $event) {
      if (isset($event['types'][$ancestor['type']])) {
        $event['inherited'] = $ancestor['type'];
        $events[$event['event']] = $event;
        $types_table = 'rpg_event_types_' . $event['event'];
        $extras = db_fetch_array(db_query("SELECT * FROM {" . $types_table . "} WHERE type='%s'", $type['type']));
        $extras = is_array($extras) ? $extras : array();
        $events[$event['event']] = array_merge($events[$event['event']], $extras);
      }
    }
  }
  return $events;
}

/**
 *  This saves a type into the database, first deleting the row, if applicable.
 *
 *  It doesn't matter if a type has been defined by a module, include file, or admin screen; they're saved
 *  the same in the database. After inserting the row, the defining module is called with an rpg_save_type hook.
 *  TODO: Not sure if the hook is actually needed, or if we want to broaden that to call all module hooks instead.
 *  TODO: attributes and actions defined on the type will be saved separately. not sure if it will call this
 *  function, or a new function specifically for those widgets.
 *
 *  @param
 *  $type
 *  An array with the type definition to save.
 */
function rpg_save_type($type) {
  db_query("DELETE FROM {rpg_types} WHERE type='%s'", $type['type']);
  db_query("INSERT INTO {rpg_types} (type, name, module, parents, shortdesc, longdesc) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", $type['type'], $type['name'], $type['module'], serialize($type['parents']), $type['shortdesc'], $type['longdesc']);

  // make sure objects inherit new values, etc.
  rpg_reset_cache();
  // reload the type, clearing the cache
  rpg_types($type['type'], false);

  // in addition to adding new types to menu, we may have changed the type name
  menu_rebuild();
}

function rpg_delete_type($type) {
  db_query("DELETE FROM {rpg_types} WHERE type='%s'", $type['type']);
  module_invoke($type['module'], 'rpg_delete_type', $type);

  // make sure objects inherit new values, etc.
  rpg_reset_cache();
  // remove old type from menu
  menu_rebuild();

  // reload the type, clearing the cache. this should also revert previously overridden module-defined types
  rpg_types($type['type'], false);}

/**
 *  implement hook_rpg_save_type
 */
function rpg_rpg_delete_type($type) {
  rpg_include_invoke($type, 'delete_type');
}

function _rpg_attribute_edit($type, $attr) {
  $type = rpg_types($type);
  $attribute = rpg_attributes($attr['attribute']);

  // TODO: move these to validation?
  if (!$type['type']) {
    drupal_set_message(t('Tried to edit attribute of non-existent type.'), 'error');
    return;
  }
  else if (!$attribute['attribute']) {
    drupal_set_message(t('Tried to edit non-existent attribute.'), 'error');
  }

  $class = rpg_attribute_classes($attribute['class']);
  $types_table = "rpg_attribute_types_" . db_escape_string($attribute['attribute']);

  $columns = array();
  $values = array();

  $columns[] = "description = '%s'";
  $values[] = $attr['description'];

  $columns[] = "get = '%s'";
  $values[] = $attr['get'];

  $columns[] = "verify = '%s'";
  $values[] = $attr['verify'];

  $columns[] = "_set = '%s'";
  $values[] = $attr['set'];

  $columns[] = "default_value = '%s'";
  $values[] = $attr['default_value'];

  $columns[] = "form_display = '%s'";
  $values[] = $attr['form_display'];

  foreach ((array)module_invoke($class['module'], 'rpg_attribute_class_settings', 'save', $class['class']) as $key => $column) {
    $columns[] = $column . " = '%s'";
    $values[$column] = $attr[$column];
  }
  foreach ((array)module_invoke($class['module'], 'rpg_attribute_class_settings', 'save serialize', $class['class']) as $key => $column) {
    $values[$column] = serialize($values[$column]);
  }

  $values[] = $type['type'];

  $col = implode(', ', $columns);

  db_query("UPDATE {" . $types_table . "} SET $col WHERE type='%s'", $values);

  $name = $edit['name'];
  if ($name && $name != $attribute['name']) {
    db_query("UPDATE {rpg_attributes} SET name = '%s' WHERE attribute='%s'", $name, $attribute['attribute']);
  }

  // rebuild the attribute & type caches
  $attribute = rpg_attributes($attribute['attribute'], false);
  rpg_types(NULL, false);

  menu_rebuild();
  return $attribute;
}

function _rpg_action_edit($type, $edit) { //$form_values) {
  $type = rpg_types($type);
  $action = rpg_actions($edit['action']);
  // TODO: move these to validation
  if (!$type['type']) {
    drupal_set_message(t('Tried to edit action of non-existent type.'), 'error');
    return;
  }
  else if (!$action['action']) {
    drupal_set_message(t('Tried to edit non-existent action.'), 'error');
  }

  $types_table = "rpg_action_types_" . db_escape_string($action['action']);

  $columns = array();
  $values = array();

  $columns[] = "description = '%s'";
  $values[] = $edit['description'];

  $columns[] = "php = '%s'";
  $values[] = $edit['php'];

  $columns[] = "expose = '%s'";
  $values[] = $edit['expose'];

  $values[] = $type['type'];

  $col = implode(', ', $columns);

  db_query("UPDATE {" . $types_table . "} SET $col WHERE type='%s'", $values);

  $name = $edit['name'];
  if ($name && $name != $action['name']) {
    db_query("UPDATE {rpg_actions} SET name = '%s' WHERE action='%s'", $name, $action['action']);
  }

  // rebuild the action & type caches
  $action = rpg_actions($action['action'], false);
  rpg_types(NULL, false);

  menu_rebuild();
  return $action;
}

function _rpg_event_edit($type, $edit) {
  $type = rpg_types($type);
  $event = rpg_events($edit['event']);
  // TODO: move these to validation
  if (!$type['type']) {
    drupal_set_message(t('Tried to edit event of non-existent type.'), 'error');
    return;
  }
  else if (!$event['event']) {
    drupal_set_message(t('Tried to edit non-existent event.'), 'error');
  }

  $types_table = "rpg_event_types_" . db_escape_string($event['event']);

  $columns = array();
  $values = array();

  $columns[] = "description = '%s'";
  $values[] = $edit['description'];

  $columns[] = "php = '%s'";
  $values[] = $edit['php'];

  $values[] = $type['type'];

  $col = implode(', ', $columns);

  db_query("UPDATE {" . $types_table . "} SET $col WHERE type='%s'", $values);

  $name = $edit['name'];
  if ($name && $name != $event['name']) {
    db_query("UPDATE {rpg_events_table} SET name = '%s' WHERE event='%s'", $name, $event['event']);
  }

  // rebuild the event & type caches
  $event = rpg_events($event['event'], false);
  rpg_types(NULL, false);

  menu_rebuild();
  return $event;
}

/**
 *  This adds a new attribute or override to a type.
 *  @param $type
 *  The name of the type to add the attribute to.
 *  @param $class
 *  The string of the class of attribute to add, such as integer or string.
 *  @param $attribute
 *  A machine-readable name for the attribute. There may only be one per type, although the same attribute may be
 *  added to further types or overridden in descended types. However, when overriding, it must be of the same class.
 *  @param $name
 *  The human-readable name of the attribute.
 */
// function rpg_attribute_add($type, $class, $attribute, $name, $build_types = false) {
function rpg_attribute_add($type, $attr, $build_types = false) {
  // load the type from the type name given
  $type = rpg_types($type);

  // load the class from the class name stored in the attribute
  $class = rpg_attribute_classes($attr['class']);

  // make sure we can write field names to the db
  $attribute = db_escape_string($attr['attribute']);

  // get our attributes human-readable name
  $name = $attr['name'];

  // test to see if we're overriding an existing attribute for this type
  $test_attribute = rpg_attributes($attribute);

  // can't add an attribute if the type or class doesn't exist
  if (!$type['type']) {
    drupal_set_message(t('Tried to add attribute to non-existent type.'), 'error');
    return;
  }
  else if (!$class['class']) {
    drupal_set_message(t('Tried to add attribute of non-existent class @class to type @type.', array('@class' => $attr['class'], '@type' => $type['name'])), 'error');
    return;
  }

  // new tables for the db
  $types_table = "rpg_attribute_types_$attribute";
  $objects_table = "rpg_attribute_rpg_$attribute";

  // only create the table if it doesn't already exist
  if (!$test_attribute['attribute']) {
    _rpg_create_attribute_tables($attribute, $class, $name, $types_table, $objects_table);
  }

  // fail if we've already created this attribute for this type -- use _rpg_attribute_edit instead ???
  if (db_result(db_query("SELECT type FROM {" . $types_table . "} WHERE type='%s'", $type['type']))) {
    drupal_set_message(t('Tried to add the @attribute attribute to the @type type where it was already defined.', array('@attribute' => $attribute, '@type' => $type['name'])), 'error');
    return;
  }

  // insert the type into the attribute's types table
  db_query("INSERT INTO {" . $types_table. "} (type) VALUES ('%s')", $type['type']);

  // rebuild the attribute & type caches
  $attribute = rpg_attributes($attribute, false);

  // we don't want a recursive call -- don't go here if we just came from here...
  if (!$build_types) {
    // make sure objects inherit new values, etc.
    rpg_reset_cache();
    rpg_types(NULL, false);
  }

  // add menu items for the new attribute
  menu_rebuild();
  return $attribute;
}


/**
 *  This adds a new action or override to a type.
 *  @param $type
 *  The name of the type to add the action to.
 *  @param $action
 *  An action array. There may only be one per type, although the same action may be
 *  added to further types or overridden in descended types.
 */
function _rpg_action_add($type, $action, $build_types = false) {
  $type = rpg_types($type);
  $action['action'] = db_escape_string($action['action']);
  if (!$type['type']) {
    drupal_set_message(t('Tried to add action to non-existent type.'), 'error');
    return;
  }
  if (!$action['action']) {
    drupal_set_message(t('Tried to add unnamed action.'), 'error');
    return;
  }

  $test_action = rpg_actions($action['action']);
  $types_table = "rpg_action_types_{$action['action']}";

  // only create the table if it doesn't already exist
  if (!$test_action['action']) {
    _rpg_create_action_tables($action['action'], $action['name'], $types_table);
  }

  if (db_result(db_query("SELECT type FROM {" . $types_table . "} WHERE type='%s'", $type['type']))) {
    drupal_set_message(t('Tried to add the @action action to the @type type where it was already defined.', array('@action' => $action['action'], '@type' => $type['name'])), 'error');
    return;
  }

  // insert the type into the action's types table
  db_query("INSERT INTO {" . $types_table. "} (type) VALUES ('%s')", $type['type']);

  // rebuild the action & type caches
  $action = rpg_actions($action['action'], false);

  // we don't want a recursive call -- don't go here if we just came from here...
  if (!$build_types) {
    // make sure objects inherit new values, etc.
    rpg_reset_cache();
    rpg_types(NULL, false);
  }

  menu_rebuild();
  return $action;
}

/**
 *  This adds a new event or override to a type.
 *  @param $type
 *  The name of the type to add the event to.
 *  @param $event
 *  An event array. There may only be one per type, although the same event may be
 *  added to further types or overridden in descended types.
 */
function _rpg_event_add($type, $event, $build_types = false) {
  $type = rpg_types($type);
  $event['event'] = db_escape_string($event['event']);
  if (!$type['type']) {
    drupal_set_message(t('Tried to add event to non-existent type.'), 'error');
    return;
  }
  if (!$event['event']) {
    drupal_set_message(t('Tried to add unnamed event.'), 'error');
    return;
  }

  $test_event = rpg_events($event['event']);
  $types_table = "rpg_event_types_{$event['event']}";

  // only create the table if it doesn't already exist
  if (!$test_event['event']) {
    _rpg_create_event_tables($event['event'], $event['name'], $types_table);
  }

  if (db_result(db_query("SELECT type FROM {" . $types_table . "} WHERE type='%s'", $type['type']))) {
    drupal_set_message(t('Tried to add the @event event to the @type type where it was already defined.', array('@event' => $event['event'], '@type' => $type['name'])), 'error');
    return;
  }

  // insert the type into the event's types table
  db_query("INSERT INTO {" . $types_table. "} (type) VALUES ('%s')", $type['type']);

  // rebuild the event & type caches
  $event = rpg_events($event['event'], false);

  // we don't want a recursive call -- don't go here if we just came from here...
  if (!$build_types) {
    // make sure objects inherit new values, etc.
    rpg_reset_cache();
    rpg_types(NULL, false);
  }

  menu_rebuild();
  return $event;
}

// TODO: examine data array
function _rpg_create_attribute_tables($attribute, $class, $name, $types_table, $objects_table) {
  // add the attribute to the attribute table
  db_query("INSERT INTO {rpg_attributes} (attribute, class, name) VALUES ('%s', '%s', '%s')", $attribute, $class['class'], $name);
  // create a new table to store type data for this attribute
  // $types_table is "rpg_attribute_types_$attribute"
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description longtext NOT NULL,
          get varchar(255) NOT NULL default '',
          verify varchar(255) NOT NULL default '',
          _set varchar(255) NOT NULL default '',
          default_value varchar(255) NOT NULL default '',
          form_display varchar(255) NOT NULL default '',
          PRIMARY KEY (type)
        ) /*!40100 DEFAULT CHARACTER SET utf8 */");
      db_query("CREATE TABLE {" . $objects_table . "} (
          rid int(10) unsigned NOT NULL,
          PRIMARY KEY (rid)
        ) /*!40100 DEFAULT CHARACTER SET utf8 */");
      break;

    case 'pgsql':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description text NOT NULL default '',
          get varchar(255) NOT NULL default '',
          verify varchar(255) NOT NULL default '',
          _set varchar(255) NOT NULL default '',
          default_value varchar(255) NOT NULL default '',
          form_display varchar(255) NOT NULL default '',
          PRIMARY KEY (type)
        )");
      db_query("CREATE INDEX {" . $types_table . "}_type_idx ON {" . $types_table . "}(type);");
      db_query("CREATE TABLE {" . $objects_table . "} (
          rid integer NOT NULL default '0',
          PRIMARY KEY (rid)
        )");
      db_query("CREATE INDEX {" . $objects_table . "}_rid_idx ON {" . $objects_table . "}(rid);");
      break;
  }
  foreach (module_invoke($class['module'], 'rpg_attribute_class_settings', 'save', $class['class']) as $value) {
    _rpg_db_add_column($types_table, $value, 'longtext');
  }
  foreach (module_invoke($class['module'], 'rpg_attribute_class_settings', 'data', $class['class']) as $column => $data) {
    _rpg_db_add_column($objects_table, $column, $data['type'], $data);
  }
}

function _rpg_create_action_tables($action, $name, $types_table) {
  // add the action to the action table
  db_query("INSERT INTO {rpg_actions} (action, name) VALUES ('%s', '%s')", $action, $name);
  // create a new table to store type data for this action
  // this will be named rpg_action_types_{$action['action']}, thus something like rpg_action_types_examine
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description longtext NOT NULL,
          php longtext NOT NULL,
          expose longtext NOT NULL,
          PRIMARY KEY (type)
        ) /*!40100 DEFAULT CHARACTER SET utf8 */");
      break;

    case 'pgsql':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description text NOT NULL default '',
          php text NOT NULL default '',
          expose text NOT NULL default '',
          PRIMARY KEY (type)
        )");
      db_query("CREATE INDEX {" . $types_table . "}_type_idx ON {" . $types_table . "}(type);");
      break;
  }
}

function _rpg_create_event_tables($event, $name, $types_table) {
  // add the event to the event table
  db_query("INSERT INTO {rpg_events_table} (event, name) VALUES ('%s', '%s')", $event, $name);
  // create a new table to store type data for this event
  // this will be named rpg_event_types_{$event['event']}, thus something like rpg_event_types_examine
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description longtext NOT NULL,
          php longtext NOT NULL,
          PRIMARY KEY (type)
        ) /*!40100 DEFAULT CHARACTER SET utf8 */");
      break;

    case 'pgsql':
      db_query("CREATE TABLE {" . $types_table . "} (
          type varchar(128) NOT NULL default '',
          description text NOT NULL default '',
          php text NOT NULL default '',
          PRIMARY KEY (type)
        )");
      db_query("CREATE INDEX {" . $types_table . "}_type_idx ON {" . $types_table . "}(type);");
      break;
  }
}

/**
 *  This loads all attributes from the database and returns the array.
 */
function _rpg_load_attributes() {
  $attributes = array();
  $results = db_query("SELECT * FROM {rpg_attributes}");
  while ($attribute = db_fetch_array($results)) {
    $attributes[$attribute['attribute']] = $attribute;
    $attributes[$attribute['attribute']]['types'] = array();
    $types = db_query("SELECT * FROM {rpg_attribute_types_" . $attribute['attribute']);
    while ($type = db_fetch_array($types)) {
      $type['set'] = $type['_set'];
      unset($type['_set']);
      $attributes[$attribute['attribute']]['types'][$type['type']] = $type;
    }
  }
  return $attributes;
}

/**
 *  This loads all actions from the database and returns the array.
 */
function _rpg_load_actions() {
  $actions = array();
  $results = db_query("SELECT * FROM {rpg_actions}");
  while ($action = db_fetch_array($results)) {
    $actions[$action['action']] = $action;
    $actions[$action['action']]['types'] = array();
    $types = db_query("SELECT * FROM {rpg_action_types_" . $action['action']);
    while ($type = db_fetch_array($types)) {
      $actions[$action['action']]['types'][$type['type']] = $type;
    }
  }
  return $actions;
}

/**
 *  This loads all events from the database and returns the array.
 */
function _rpg_load_events() {
  $events = array();
  $results = db_query("SELECT * FROM {rpg_events_table}");
  while ($event = db_fetch_array($results)) {
    $events[$event['event']] = $event;
    $events[$event['event']]['types'] = array();
    $types = db_query("SELECT * FROM {rpg_event_types_" . $event['event']);
    while ($type = db_fetch_array($types)) {
      $events[$event['event']]['types'][$type['type']] = $type;
    }
  }
  return $events;
}

/**
 * Add a column to a database table.
 *  This is lifted straight from cck -- content_admin.inc
 *
 * @param $table
 *   Name of the table, without {}
 * @param $column
 *   Name of the column
 * @param $type
 *   Type of column
 * @param $attributes
 *   Additional optional attributes. Recognized attributes:
 *     not null => TRUE|FALSE
 *     default  => NULL|FALSE|value (with or without '', it won't be added)
 */
function _rpg_db_add_column($table, $column, $type, $attributes = array()) {
  switch ($GLOBALS['db_type']) {
    case 'pgsql':
      $mappings = array('int' => 'integer', 'mediumint' => 'integer', 'bigint' => 'integer',
        'tinyint' => 'smallint',
        'float' => 'float',
        'varchar' => 'varchar',
        'text' => 'text', 'mediumtext' => 'text', 'longtext' => 'text');
      if (isset($mappings[$type])) {
        $type = $mappings[$type];
      }
      else {
        watchdog('database', t('No PostgreSQL mapping found for %type data type.', array('%type' => $type)), WATCHDOG_WARNING);
      }
      if ($type != 'varchar') {
        unset($attributes['length']);
      }
      break;
  }

  if (array_key_exists('not null', $attributes) && $attributes['not null']) {
    $not_null = 'NOT NULL';
  }
  if (array_key_exists('default', $attributes)) {
    if (is_null($attributes['default'])) {
      $default_val = 'NULL';
      $default = 'default NULL';
    }
    elseif ($attributes['default'] === FALSE) {
      $default = '';
    }
    else {
      $default_val = "$attributes[default]";
      $default = "default $attributes[default]";
    }
  }
  if (array_key_exists('length', $attributes)) {
    $type .= '('. $attributes['length'] .')';
  }
  if (array_key_exists('unsigned', $attributes) && $attributes['unsigned']) {
    switch ($GLOBALS['db_type']) {
      case 'pgsql':
        $type = str_replace('integer', 'int_unsigned', $type);
        break;
      default:
        $type .= ' unsigned';
        break;
    }
  }
  switch ($GLOBALS['db_type']) {
    case 'pgsql':
      db_query("ALTER TABLE {". $table ."} ADD $column $type");
      if ($default) {
        db_query("ALTER TABLE {". $table ."} ALTER $column SET $default");
      }
      if ($not_null) {
        if ($default) {
          db_query("UPDATE {". $table ."} SET $column = $default_val");
        }
        db_query("ALTER TABLE {". $table ."} ALTER $column SET NOT NULL");
      }
      break;

    case 'mysql':
    case 'mysqli':
      // MySQL allows no DEFAULT value for text (and blob) columns
      if (in_array($type, array('text', 'mediumtext', 'longtext'))) {
        $default = '';
        // We also allow NULL values to account for CCK's per field INSERTs
        $not_null = '';
      }
      db_query('ALTER TABLE {'. $table .'} ADD COLUMN '. $column .' '. $type .' '. $not_null .' '. $default);
      break;
  }
}

/**
 * Change a column definition.
 *
 * Remember that changing a column definition involves adding a new column
 * and dropping an old one. This means that any indices, primary keys and
 * sequences from serial-type columns are dropped and might need to be
 * recreated.
 *
 *  This is lifted straight from cck -- content_admin.inc
 *
 * @param $table
 *   Name of the table, without {}
 * @param $column
 *   Name of the column to change
 * @param $column_new
 *   New name for the column (set to the same as $column if you don't want to change the name)
 * @param $type
 *   Type of column
 * @param $attributes
 *   Additional optional attributes. Recognized attributes:
 *     not null => TRUE|FALSE
 *     default  => NULL|FALSE|value (with or without '', it won't be added)
 */
function _rpg_db_change_column($table, $column, $column_new, $type, $attributes = array()) {
  switch ($GLOBALS['db_type']) {
    case 'pgsql':
      $mappings = array('int' => 'integer', 'mediumint' => 'integer', 'bigint' => 'integer',
        'tinyint' => 'smallint',
        'float' => 'float',
        'varchar' => 'varchar',
        'text' => 'text', 'mediumtext' => 'text', 'longtext' => 'text');
      if (isset($mappings[$type])) {
        $type = $mappings[$type];
      }
      else {
        watchdog('database', t('No PostgreSQL mapping found for %type data type.', array('%type' => $type)), WATCHDOG_WARNING);
      }
      if ($type != 'varchar') {
        unset($attributes['length']);
      }
      break;

    case 'mysql':
    case 'mysqli':
      break;
  }

  if (array_key_exists('not null', $attributes) and $attributes['not null']) {
    $not_null = 'NOT NULL';
  }
  if (array_key_exists('default', $attributes)) {
    if (is_null($attributes['default'])) {
      $default_val = 'NULL';
      $default = 'default NULL';
    }
    elseif ($attributes['default'] === FALSE) {
      $default = '';
    }
    else {
      $default_val = "$attributes[default]";
      $default = "default $attributes[default]";
    }
  }
  if (array_key_exists('length', $attributes)) {
    $type .= '('. $attributes['length'] .')';
  }
  if (array_key_exists('unsigned', $attributes) && $attributes['unsigned']) {
    switch ($GLOBALS['db_type']) {
      case 'pgsql':
        $type = str_replace('integer', 'int_unsigned', $type);
        break;
      default:
        $type .= ' unsigned';
        break;
    }
  }

  switch ($GLOBALS['db_type']) {
    case 'pgsql':
      db_query("ALTER TABLE {". $table ."} RENAME $column TO ". $column ."_old");
      db_query("ALTER TABLE {". $table ."} ADD $column_new $type");
      db_query("UPDATE {". $table ."} SET $column_new = ". $column ."_old");
      if ($default) {
        db_query("ALTER TABLE {". $table ."} ALTER $column_new SET $default");
      }
      if ($not_null) {
        db_query("ALTER TABLE {". $table ."} ALTER $column_new SET NOT NULL");
      }
      db_query("ALTER TABLE {". $table ."} DROP ". $column ."_old");
      break;

    case 'mysql':
    case 'mysqli':
      // MySQL allows no DEFAULT value for text (and blob) columns
      if (in_array($type, array('text', 'mediumtext', 'longtext'))) {
        $default = '';
        // We also allow NULL values to account for CCK's per field INSERTs
        $not_null = '';
      }
      db_query('ALTER TABLE {'. $table .'} CHANGE '. $column .' '. $column_new .' '. $type .' '. $not_null .' '. $default);
      break;
  }
}


/**
 *  This will allow an object to register itself in the database as being the player character of a user.
 *
 *  @param $rid
 *  The rid of the object to register
 *  @param $uid
 *  The drupal user/account uid of the user to register
 *  @return
 *  TRUE or FALSE depending on whether successful or not
 */
function _rpg_register_object_as_pc($rid, $uid) {
  $object = rpg_object($rid);
  $account = user_load(array('uid' => $uid));
  if ($account->uid && $object->rid) {
    db_query("DELETE FROM {rpg_pc} WHERE uid=%d AND rid=%d", $account->uid, $object->rid);
    db_query("INSERT INTO {rpg_pc} (uid, rid) VALUES (%d, %d)", $account->uid, $object->rid);
    return TRUE;
  }
  return FALSE;
}

/**
 *  This will allow an object to remove itself from the database as being the player character of a user.
 *
 *  @param $rid
 *  The rid of the object to register
*  @param $uid
 *  The drupal user/account uid of the user to register
 *  @return
 *  TRUE or FALSE depending on whether successful or not
 */
function _rpg_remove_object_as_pc($rid, $uid) {
  $object = rpg_object($rid);
  $account = user_load(array('uid' => $uid));
  if ($account->uid && $object->rid) {
    db_query("DELETE FROM {rpg_pc} WHERE uid=%d AND rid=%d", $account->uid, $object->rid);
    return TRUE;
  }
  return FALSE;
}

