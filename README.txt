
RPG

This module provides a base for creating an online role-playing game, at its
basic similar in nature to Lord of the Red Dragon or Urban Dead, but as
development progresses, able to handle increasingly complex games including
graphics and jquery. Being integrated with Drupal, it will allow administrators
to fully utilize other features of Drupal, integrating forums, organic groups,
and more with the RPG.

NOTE: This engine is still in development. Not only does it not do anything at
the moment (besides the admin screens), it is likely to change drastically as
time goes on.

IMPORTANT: Go to DrupalRPG.org and the Drupal Games and Game APIs group page
for ongoing discussions and plans concerning the progress of this module.

RPG

Created by Aaron Winborn
aaron at advomatic dot com
Begun 6 December 2006
Rewrite of engine 12 April 2007
Begun support for rulesets 18 August 2007

The engine itself does little more than provide a core to build a game with.
Although it is intended to be for online RPG's, there is no reason the base
couldn't be expanded to create other types of games, such as Turn-Based
Strategy games, RTS games, or even slidescrollers. The engine is able to do
this by providing basic functionality for administrators to create, use, and
share 'rulesets'.

Rulesets are definitions of the Types, Attributes, and Actions used to create
in-game objects. They may be created in forms available to game administrators,
and may also be imported/exported from forms or even files. RPG Object Types
follow inheritance rules, and may even include multiple parents, allowing types
to share and even override attributes and actions.

Types

All in-game objects are derived from an RPG type. Types include anything from an
abstract 'tangible' object, to a 'weapon' that 'swords' or 'hammers' inherit, to
an 'orc' that could be used as an NPC or a user's PC. Types follow inheritance
rules, so for instance, you might have tangible -> weapon -> sword -> flaming
sword, or something more complex where in addition to the 'sword' that 'flaming
sword' derives from, it might have an additional parent of 'magical item', so that
it inherits attributes and actions from both types, and so on down the line.

Types will inherit the Attributes and Actions of all its parents (and their parents,
etc.). In addition to defining new Attributes and Actions, they may override any from
their parent types. For instance, the aforementioned magic sword might inherit the
action 'take sword' from the 'tangible' type, the action 'swing sword at $target'
from 'weapon', and 'charge sword mana' from 'magical item'. It might furthermore
override the 'charge sword mana', which might normally allow charging by any wizard,
to only allow charging by a wizard with a weaponsmith skill.

Types may be defined and modified from various admin screens, available from
/admin/rpg/types.

Attributes

Most RPG object types will define and/or inherit attributes. Attributes will be stored
for individual objects, so that each object will (usually) have its own unique values
for the attributes defined by its type.

Attributes will be one of several 'classes'. Several classes come with the RPG module,
and others may be defined by other contributed or custom modules. The attributes
shipped with the module include 'text', 'number', 'boolean', 'object array', 'figured',
'sprite', 'node', 'view' and 'link'.

The text, number, and boolean attributes are what they sound like. Text attributes
will store unique text for the object, such as a name, description, or message to be
displayed when the object explodes. Number attributes currently only store integers.
Booleans are simple true or false values, set by a checkbox in the admin screens.
Object arrays will store an array of rpg objects, and are useful for such things as
a list of items contained within a chest, or a list of wizards who have read a
certain scroll.

Figured attributes may not be set for an individual object, but rather are figured
on the fly. For instance, 'mobile actors' might have 'max hit points' that are
figured by a combination of 'class', 'race', 'health', and 'level'. They are
defined as a snippet of php code that will be evaluated as needed.

In actuality, any attribute may make use of this code; for instance, when setting
'location', the snippet might also ensure that an object is removed from it's old
location's 'contents', and added to the new. However, the technical difference
with figured attributes is that they do not store a value in the database. For
those technically minded, there are three php snippets available to all attributes:
'get', 'verify', and 'set'. When present in a type, 'get' is evaluated before an
attribute's value is called for an object, 'verify' just before setting the value
(which will fail if it returns false, thus not setting the new value), and 'set',
which is called just before setting the new value (and if provided, the new value
will be set to the number returned by that snippet).

Continuing through the attribute classes, 'sprites' are images (or other files,
such as animated gifs or embedded flash objects) that may be displayed as needed.
For instance, chests might have a static image, potions might have an animated
gif, and orcs might have animated gifs that change direction as the orc moves in
a certain direction.

'Node' classed attributes reference Drupal nodes. This might be used to embed a
link to a group page for assassins, to display a news item teaser, to show an ad
for a book sold by the site, or many other creative uses. Likewise, 'view'
attributes reference a View on the site, assuming the Views module is installed.
Finally, 'link' is any URL, internal or external, for similar purposes.

Actions

Actions are used to affect the game state of objects. They may be called by players
on the site, or internally by other actions, depending on their definition.

For instance, our flaming sword type might inherit an action of 'take sword'. This
action would first check that the sword may be taken by the player, which is a
figured attribute called 'may_take' (which in turn examines 'moveable' and makes
certain the object is in the same room as the player). Assuming this check passes,
the sword is moved to the player's inventory.

Another action might be 'wield sword', which by default might simply set the
'wielded' attribute to true, assuming it's in the player's inventory. However, the
flaming sword might override this behavior, so that if a player is an orc, the
player might be damaged when grasping its hilt.

Objects

In code, objects are always referenced by their id, and never directly. Functions
are provided for this, such as rpg_get($object, $attribute) and rpg_set($object,
$attribute, $value). This is particularly important with setting attributes -- this
ensures the new value will be saved to the database.

Meta Game

Rulesets may also define their own meta game values. This includes how often a
turn takes place, how many 'action points' are available to characters, how many
characters a player may use, what the weather is like, etc. As with every other
aspect of the RPG module, these values may be changed from various admin forms and
by importing/exporting rulesets.

Drudge Ruleset

The RPG currently is shipped with the Drudge ruleset. This is a generic ruleset,
defining some basic object types, skills, and simple actions. It is meant to be
built upon for an individual game's flavor, but is also meant to be robust enough
to be meaningful with little modification.

Other rulesets may be available in the future. Admins are encouraged to contribute
their own rulesets. Please see the discussions at DrupalRPG.org and the Drupal Games
and Game APIs group page for more information.
