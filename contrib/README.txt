
=======================
RPG Contributed Modules
=======================

These are modules that depend on the RPG module, but are not necessarily required to run an RPG.

================
RPG Base Classes
================

This module provides the following base classes with which to create attributes.

Number: This will store an integer.
Text: This will store a raw text value. Make sure to run through check_plain, etc. before displaying.
Boolean: This will store true/false.
Array: This will store an array made up of any values.
Figured: This will not store any value, and is meant to be processed exclusively with the 'get' function.
Node: This will store the nid of a Drupal node object.
User: This will store the uid of a Drupal user object.
Object: This will store the rid of an RPG object.
Object Array: This will store an array made up only of RPG objects.

============
RPG Rulesets
============

This module allows Rulesets to be imported & exported, both by contributed Ruleset modules (such as RPG DRUDGE), and by admin screens
that import/export with eval'd php scripts.

You can import a new Ruleset module by visiting /admin/rpg/rulesets, and selecting the Ruleset module you would like to import. This
will take you to a screen with the types that may be imported, where you check the types you would like. If a type depends on another
Ruleset module type that has not yet been loaded, then that type will be grayed out.

After importing types, they will be available from the RPG Types admin screens, and may be modified as normal for other types. However,
make sure not to turn off the Ruleset module once you've imported its types (unless you know precisely what you're doing): most of these
types depend on functions defined in that module. However, you may safely turn off the RPG Rulesets module after importing, if you
desire.

To export types, go to the Export Ruleset tab of the Types admin screen. There, you will see a listing of all types defined in the system.
Check the types you wish to export, and hit submit. This will create a textarea with the php script required to define those types,
which may then be copied and pasted into the Import Ruleset tab on another site, or saved to be distributed as desired.

===========
RPG Message
===========

This allows messages to be displayed to a character. They will normally be displayed in a block (or other method) on page load,
generally at rpg/play. Messages will be displayed to all 'listeners' of an action or event, and may be modified by other modules.

=========
RPG Event
=========

RPG in-game events are processed in a queue. New events are added to the queue. Each event keeps track of when the event was created,
when it should be fired, what function to call, what object fired the event, and what args to call the event with. It also allows for
turns in a game, rather than using system time.

==========
RPG Action
==========

This allows actions to be created by players and NPC's, which will be processed as events.
