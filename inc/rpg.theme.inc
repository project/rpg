<?php

function theme_rpg_play($pc) {
  $output = t('You are playing !pc.', array('!pc' => l(rpg_get('name', $pc), 'rpg/view/' . rpg_id($pc))));
  return $output;
}

function theme_rpg_character_select_page($account, $pcs) {
  $items = array();
  if (count($pcs)) {
    foreach ($pcs as $pc) {
      $items[] = l(rpg_get('name', $pc), 'rpg/select/' . $pc);
    }
  }
  $output .= theme('item_list', $items);
  $output .= l(t('Create Character'), 'rpg/create/pc');
  return $output;
}

function theme_rpg_create_pc_page($pc, $type, $form) {
  drupal_set_title(t('Create @type Player Character', array('@type' => $type['name'])));
  $output .= $type['longdesc'];
  $output .= $form;
  return $output;
}

/**
 *  display the form for object creation
 *  @param $object
 *    the newly created object with appropriate properties already populated for the type
 *  @param $type
 *    the type array for the object
 *  @param $form
 *    the form to display
 */
function theme_rpg_create_object_page($object, $type, $form) {
  drupal_set_title(t('Create @type', array('@type' => $type['name'])));
  $output .= $type['longdesc'];
  $output .= $form;
  return $output;
}

/**
 *  display the form for editing an rpg object
 *  @param $object
 *    the loaded object
 *  @param $form
 *    the form to display
 */
function theme_rpg_edit_object_page($object, $form) {
  drupal_set_title(t('Edit @obj', array('@obj' => rpg_get('name', $object))));
  $output .= $form;
  return $output;
}

/**
 *  display the form for viewing an rpg object
 *  @param $object
 *    the loaded object
 *  TODO: hooks
 *  TODO: action links
 */
function theme_rpg_view_object_page($object) {
  $name = rpg_get('name', $object);
  $title = t('@name', array('@name' => $name));
  drupal_set_title($title);
  $output .= '<div class="rpg_view_object">';
  $output .= '<div class="content">';
  $output .= rpg_get('description', $object);
  $output .= '</div><!--close content-->';

//  $links = rpg_invoke($object, 'actions', $rpg['pc']);
  // TODO: rpg_edit_access???
  if (user_access('administer rpg')) {
    $links[] = l(t('edit @name', array('@name' => $name)), 'rpg/edit/' . rpg_id($object));
  }
  if (!empty($links)) {
    $output .= '<div class="links">';
    $output .= theme('item_list', $links);
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}

function theme_rpg_admin_types($types) {
  $output .= t('The following types are available for creating objects in the RPG. You may edit, delete, or add to the types from the following table:');
  $header = array(t('Type'), t('Description'), t('Operations'));
  $rows = array();
  foreach($types as $type) {
    $row = array();
    $row[] = l($type['name'], 'admin/rpg/types/type/' . $type['type']);
    $row[] = t('@desc', array('@desc' => $type['shortdesc']));
    $actions = array();
    $actions[] = l(t('edit'), 'admin/rpg/types/type/' . $type['type']);
    $actions[] = l(t('delete'), 'admin/rpg/types/type/' . $type['type'] . '/delete');
    $actions[] = l(t('attributes'), 'admin/rpg/types/type/' . $type['type'] . '/attributes');
    $actions[] = l(t('actions'), 'admin/rpg/types/type/' . $type['type'] . '/actions');
    $actions[] = l(t('events'), 'admin/rpg/types/type/' . $type['type'] . '/events');
    $row[] = implode(', ', $actions);
    $rows[] = $row;
  }
  $output .= theme('table', $header, $rows);
  $output .= l(t('Add new RPG type'), 'admin/rpg/types/add');
  return $output;
}

function theme_rpg_browse_page ($objects) {
  $items = array();
  foreach ($objects as $object) {
    $object = rpg_object($object);
    $name = rpg_get('name', $object->rid);
    $items[] = l($name ? $name : $object->rid, 'rpg/view/' . $object->rid) . '<br />' . rpg_get('description', $object);
  }
  $output .= theme('item_list', $items);
  $output .= theme('pager');
  return $output;
}

/**
 *  This will display the intro text for rpg. In general, you should either override this theme function in your template.php file,
 *  or override the /rpg path entirely, pointing to your own node page, panel, view, or what have you.
 */
function theme_rpg_page() {
  $output .= t('Welcome to Drupal RPG! Things are still in development, so !play at your own risk. If you are the administrator, you will probably want to create a new theme function to override this message. See the Drupal Handbook for more help.', array('!play' => l(t('play'), 'rpg/play')));
  return $output;
}

function theme_rpg_get($property, $object) {
  $args = func_get_args();
  $attribute = rpg_object_attributes($object);
  if ($attribute['theme']) {
    $output = call_user_func_array('theme', array_merge(array($attribute['theme']), $args));
  }
  if (!$output) {
    foreach (rpg_object_types($object) as $type) {
      $output = call_user_func_array('theme', array_merge(array("rpg_get_type_{$type}_attribute_$property"), $args));
      if ($output) {
        break;
      }
    }
  }
  if (!$output) {
    $output = call_user_func_array('theme', array_merge(array("rpg_get_attribute_$property"), $args));
  }
  if (!$output) {
    $output = call_user_func_array('theme', array_merge(array('rpg_get_default'), $args));
  }
  return $output;
}

function theme_rpg_get_default($property, $object) {
  $args = func_get_args();
  return check_plain(call_user_func_array('rpg_get', $args));
}
