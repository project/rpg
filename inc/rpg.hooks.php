<?php

/**
 *  @file
 *  This is the API for rpg hooks.
 *  These functions are not actually called directly, but are rather hooks to be used in contributed modules,
 *  that will be called as appropriate from internal RPG functions. Examples are given for the benefit of
 *  contributing programmers. As with all Drupal hooks, replace 'hook' with the internal name of the module.
 */

/**
 *  This is called when a new or edited RPG type is saved. Use for saving extra data to the database, etc.
 *  This hook is only called from the module defining that type. It is called both when a type is added programmatically,
 *  and when defined from an admin form. It is called directly after the type is inserted or updated in the database,
 *  but before the global rpg_types array is updated.
 *
 *  @ingroup hooks
 */
function hook_rpg_save_type($type) {
}

/**
 *  This will load any module-defined custom types, for use by other types and RPG objects.
 *  This function is called when loading rpg types into the system. Note that types are cached, and that admins may override
 *  or delete types manually. Thus, this function is only called when initially creating/updating the type, and may be
 *  overridden later.
 *
 *  @ingroup hooks
 */
function hook_rpg_load_types() {
}

/**
 *  This hook defines various classes for attributes, such as Number, Text, and Figured. When an attribute is created,
 *  this hook is called according to the assigned class. It will determine what columns to add to dynamically created
 *  tables for both types and objects, and also create form elements for admin display.
 *
 *  @param $op
 *    This determines the operation being called. Must be a string of one of the following values:
 *      'save': This defines the columns to save for each type. Must return an array of column names as strings.
 *      'data': This defines the columns to save object-specific data. Must return an associative array in the
 *        following format: $data['column-name'] = array('type' => 'int|text|varchar', ['not null' => true|false,
 *        'default' => true|false, 'sortable' => true|false, etc.]);
 *        The values passed will be used to create new columns in the rpg_attribute_rpg_[attribute-name] table.
 *      'form': This defines form elements to display to the admin when adding/editing the type form.
 *
 *  @param $class
 *    This is the string name of the class, such as 'number' or 'text'.
 *
 *  @param $attribute = NULL
 *    This is an array defining the attribute in question.
 *
 *  @return
 *    The return value depends on the $op. An array is expected in each case, but see above for the values expected.
 *
 *  @ingroup hooks
 */
function hook_rpg_attribute_class_settings($op, $class, $attribute = NULL) {
}

/**
 *  This hook is used to define what rpg types may be used as player characters for users to choose from.
 *
 *  Typically used by Rulesets, but any module may use the hook. This is used primarily to create the lists
 *  from /rpg/create/pc.
 *
 *  @param $user
 *    The user creating the PC. This may be used to limit the available PC types, according to the user's role access settings,
 *    or other criteria, such as whether a user has achieved a certain quest with another character.
 *  @return
 *    An array of types that may be used when creating pc's
 *
 *  @ingroup hooks
 */
function hook_rpg_pc_types($user) {
  $current_pcs = rpg_user_pcs($user);
  if (sizeof($current_pcs) >= 5 && !user_access('administer rpg')) {
    // don't allow users more than 5 characters
    return array();
  }
  $types = array('human', 'robot', 'elf' => 'elf');
  if (user_access('super rpg')) {
    $types[] = 'giant';
  }
  if (in_array('power gamer', $user->roles)) {
    $types[] = 'vampire_cyborg';
  }
  foreach ($current_pcs as $pc) {
    if (module_invoke('rpg_achieve', 'get', 'traversed_labrynth', $pc)) {
      $types['minotaur'] = 'minotaur';
    }
    // only allow one elf per user
    if (rpg_is_a($pc, 'elf')) {
      unset($types['elf']);
    }
  }
  return $types
}

/**
 *  Messages are passed using a data structure, which may be altered using hook_rpg_message_alter.
 *
 *  Messages are an array in the following form:
 *    '#mid' => the unique message id of the message
 *    '#created' => the time of creation of the message
 *    '#action' => the action originally defining the message
 *    '#message' => the default message to be output, ultimately sent through t() using the other parameters
 *    '#who' => who is performing the action (defaults to $rpg['pc'])
 *    '#target' => the target of the action
 *    '#with' => what who is using to do action to target
 *    '#listeners' => an array of objects who receive the message as observers
 *    '#room' => the room where the action takes place (defaults to #who's top room)
 *    '#who_message' => the message to be displayed to #who (overriding #message if provided)
 *    '#target_message' => the message to be displayed to #target (overriding #message if provided)
 *    '#what_message' => the message to be displayed to #what (overriding #message if provided)
 *    '#observer_message' => the message to be displayed to anyone else viewing the action (overriding #message if provided)
 *    '#args' => an array of extra args to be sent to the resulting t() message, in the form of '!key' => l('!key', 'url/' . $key).
 *      this will be done automatically for who, target, with, and room.
 *
 *  Examples:
 *    using the 'take' action:
 *    $message = array(
 *      '#action' => 'take',
 *      '#message' => '!who takes !target.',
 *      '#who' => $who,
 *      '#target' => $target,
 *      '#who_message' => 'You take !target.',
 *      '#target_message' => '!who takes you.'
 *    );
 *    This will ultimately resolve to
 *      t('!who takes !target', array('!who' => l(rpg_get('name', $who), 'rpg/view/' . $who->rid), '!target' => l(rpg_get('name', $target), 'rpg/view/' . $target->rid)))
 *
 *  @param $message
 *    The message data structure, as defined above.
 */
function hook_rpg_message_alter(&$message) {
  if ($message['#action'] == 'take') {
    if (rpg_get('location', $message['#target']) == rpg_get('top_room', $message['#who'])) {
      $message['#message'] = '!who lifts !target from the floor.';
      $message['#who_message'] = 'You lift !target from the floor.';
      $message['#target_message'] = '!who lifts you from the floor.';
    }
  }
}


function hook_rpg_object_alter(&$object, $op) {
  switch ($op) {
    case 'load':
      break;
  }
}
