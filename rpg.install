<?php

function rpg_install() {
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      // this stores all object information
      db_query("
        CREATE TABLE {rpg} (
          rid int(10) NOT NULL default '0',
          uid int(10) NOT NULL default '0',
          created int(11) NOT NULL default '0',
          changed int(11) NOT NULL default '0',
          data longtext NOT NULL,
          PRIMARY KEY (rid),
          KEY uid (uid)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this stores all the types of an object
      db_query("
        CREATE TABLE {rpg_object_types} (
          rid int(10) NOT NULL default '0',
          type varchar(128) NOT NULL default '',
          KEY rid (rid),
          KEY type (type)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this stores meta info about object types: more tables are created dynamically
      db_query("
        CREATE TABLE {rpg_types} (
          type varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          module varchar(128) NOT NULL default '',
          parents longtext NOT NULL,
          shortdesc longtext NOT NULL,
          longdesc longtext NOT NULL,
          data longtext NOT NULL,
          PRIMARY KEY (type),
          KEY name (name),
          KEY module (module)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this stores meta info about attributes: more tables are created dynamically
      db_query("
        CREATE TABLE {rpg_attributes} (
          attribute varchar(128) NOT NULL default '',
          class varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (attribute)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this stores meta info about actions: more tables are created dynamically
      db_query("
        CREATE TABLE {rpg_actions} (
          action varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (action)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this stores meta info about events: more tables are created dynamically
      db_query("
        CREATE TABLE {rpg_events_table} (
          event varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (event)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this is a many-to-many table of objects that are pc objects (may be subject of rpg/play). keyed to drupal user
      db_query("
        CREATE TABLE {rpg_pc} (
          uid int(10) NOT NULL default '0',
          rid int(10) NOT NULL default '0',
          active int(2) NOT NULL default '0',
          KEY uid (uid),
          KEY rid (rid)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      // this is the rpg cache table.
      db_query("
        CREATE TABLE {cache_rpg} (
          cid varchar(255) NOT NULL default '',
          data longblob,
          expire int(11) NOT NULL default '0',
          created int(11) NOT NULL default '0',
          headers text,
          PRIMARY KEY (cid),
          INDEX expire (expire)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      break;
    case 'pgsql':
      // these are the same tables as above, but for pgsql. love d6...
      db_query("
        CREATE TABLE {rpg} (
          rid integer NOT NULL default '0',
          uid integer NOT NULL default '0',
          created integer NOT NULL default '0',
          changed integer NOT NULL default '0',
          data text NOT NULL default '',
          PRIMARY KEY (rid)
        );
      ");
      db_query("CREATE INDEX {rpg}_rid_idx ON {rpg}(rid);");
      db_query("CREATE INDEX {rpg}_uid_idx ON {rpg}(uid);");
      db_query("
        CREATE TABLE {rpg_object_types} (
          rid integer NOT NULL default '0',
          type varchar(128) NOT NULL default ''
        );
      ");
      db_query("CREATE INDEX {rpg_object_types}_rid_idx ON {rpg_object_types}(rid);");
      db_query("CREATE INDEX {rpg_object_types}_type_idx ON {rpg_object_types}(type);");
      db_query("
        CREATE TABLE {rpg_types} (
          type varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          module varchar(128) NOT NULL default '',
          parents text NOT NULL default '',
          shortdesc text NOT NULL default '',
          longdesc text NOT NULL default '',
          data text NOT NULL default '',
          PRIMARY KEY (type)
        );
      ");
      db_query("CREATE INDEX {rpg_types}_type_idx ON {rpg_types}(type);");
      db_query("CREATE INDEX {rpg_types}_name_idx ON {rpg_types}(name);");
      db_query("CREATE INDEX {rpg_types}_module_idx ON {rpg_types}(module);");
      db_query("
        CREATE TABLE {rpg_attributes} (
          attribute varchar(128) NOT NULL default '',
          class varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (attribute)
        );
      ");
      db_query("CREATE INDEX {rpg_attributes}_attribute_idx ON {rpg_attributes}(attribute);");
      db_query("
        CREATE TABLE {rpg_actions} (
          action varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (action)
        );
      ");
      db_query("CREATE INDEX {rpg_actions}_action_idx ON {rpg_actions}(action);");
      db_query("
        CREATE TABLE {rpg_events_table} (
          event varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (action)
        );
      ");
      db_query("CREATE INDEX {rpg_events_table}_event_idx ON {rpg_events_table}(event);");
      db_query("
        CREATE TABLE {rpg_pc} (
          uid integer NOT NULL default '0',
          rid integer NOT NULL default '0',
          active integer NOT NULL default '0'
        );
      ");
      db_query("CREATE INDEX {rpg_pc}_uid_idx ON {rpg_pc}(uid);");
      db_query("CREATE INDEX {rpg_pc}_rid_idx ON {rpg_pc}(rid);");
      db_query("
        CREATE TABLE {cache_rpg} (
          cid varchar(255) NOT NULL default '',
          data bytea default '',
          expire integer NOT NULL default '0',
          created integer NOT NULL default '0',
          headers text default '',
          PRIMARY KEY (cid)
        );
      ");
      db_query("CREATE INDEX {rpg_cache}_expire_idx ON {rpg_cache}(expire);");
      break;
  }
}

/**
 *  move types to their own table
 */
function rpg_update_1() {
  $ret = array();
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $ret[] = update_sql("
        CREATE TABLE {rpg_object_types} (
          rid int(10) NOT NULL default '0',
          type varchar(128) NOT NULL default '',
          KEY rid (rid),
          KEY type (type)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      break;
    case 'pgsql':
      $ret[] = update_sql("
        CREATE TABLE {rpg_object_types} (
          rid integer NOT NULL default '0',
          type varchar(128) NOT NULL default ''
        );
      ");
      $ret[] = update_sql("CREATE INDEX {rpg_object_types}_rid_idx ON {rpg_object_types}(rid);");
      $ret[] = update_sql("CREATE INDEX {rpg_object_types}_type_idx ON {rpg_object_types}(type);");
      break;
  }
  $results = db_query("SELECT rid, types FROM {rpg}");
  while ($object = db_fetch_object($results)) {
    foreach (unserialize($object->types) as $type) {
      $ret[] = update_sql("INSERT INTO {rpg_object_types} (rid, type) VALUES (" . $object->rid . ", '" . db_escape_string($type) . "')");
    }
  }
  $ret[] = update_sql("ALTER TABLE {rpg} DROP types");
  return $ret;
}

function rpg_update_2() {
   $ret = array();
   $ret[] = update_sql("DELETE FROM {cache_rpg}");
   return $ret;
}

/**
 *  set form_display in action/attribute tables
 */
function rpg_update_3() {
  $changes = "ADD COLUMN form_display varchar(255) NOT NULL default ''";
  $ret = _rpg_update_attribute_table_alter($changes);
  $ret[] = update_sql("DELETE FROM {cache_rpg}");
  return $ret;
}

function _rpg_update_attribute_table_alter($changes) {
  include_once('./'. drupal_get_path('module', 'rpg') .'/rpg.module');
  $ret = array();
  $attributes = rpg_attributes();
  foreach ($attributes as $attribute) {
    $type_table = "rpg_attribute_types_{$attribute['attribute']}";
    $ret[] = update_sql("ALTER TABLE {" . $type_table . "} $changes");
  }
  return $ret;
}

function _rpg_update_action_table_alter($changes) {
  include_once('./'. drupal_get_path('module', 'rpg') .'/rpg.module');
  $ret = array();
  $actions = rpg_actions();
  foreach ($actions as $action) {
    $type_table = "rpg_action_types_{$action['action']}";
    $ret[] = update_sql("ALTER TABLE {" . $type_table . "} $changes");
  }
  $ret[] = update_sql("DELETE FROM {cache_rpg}");
  return $ret;
}

function rpg_update_4() {
  $ret = array();
  $ret[] = update_sql("DROP TABLE {rpg_tangible}");
  return $ret;
}

function rpg_update_5() {
  $ret = array();
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      $ret[] = update_sql("
        CREATE TABLE {rpg_events_table} (
          event varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (event)
        ) /* !40100 DEFAULT CHARACTER SET UTF8 */;
      ");
      break;
    case 'pgsql':
      $ret[] = update_sql("
        CREATE TABLE {rpg_events_table} (
          event varchar(128) NOT NULL default '',
          name varchar(128) NOT NULL default '',
          PRIMARY KEY (event)
        );
      ");
      $ret[] = update_sql("CREATE INDEX {rpg_events_table}_event_idx ON {rpg_events_table}(event);");
      break;
  }
  return $ret;
}
